<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');


Route::group(['middleware'=>'checklogin'],function(){
    Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function(){
        $prefix = "category";
        $controller = "category";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
                $controllerName = ucfirst($controller) . "Controller@";
                Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
                Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
                Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
                Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
                Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
                Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
                Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
                Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
                Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
                Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
                Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
                Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
                Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
                Route::get('select/{status}', $controllerName . 'select')->name('select');
            });
        $prefix = "post";
        $controller = "post";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "tag";
        $controller = "tag";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus/{id}', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::post('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "user";
        $controller = "user";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
            Route::get('editPass/{id}', $controllerName . 'editPass')->name('editPass')->middleware("can:".$controller.".edit");;
            Route::post('changePass', $controllerName . 'changePass')->name('changePass')->middleware("can:".$controller.".edit");;
        });
        $prefix = "role";
        $controller = "role";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "banner";
        $controller = "banner";
            Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "comment";
        $controller = "comment";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "contact";
        $controller = "contact";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "info";
        $controller = "info";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
        });
        $prefix = "menu";
        $controller = "menu";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "policy";
        $controller = "policy";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "product";
        $controller = "product";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
            Route::get('updatePrice', $controllerName . 'updatePrice')->name('updatePrice');
            Route::get('updateDiscount', $controllerName . 'updateDiscount')->name('updateDiscount');
            Route::get('importExcel',   $controllerName . 'importExcel')->name('importExcel')->middleware("can:".$controller.".add");
            Route::post('storefromExcel', $controllerName . 'storefromExcel')->name('storefromExcel')->middleware("can:".$controller.".add");
            Route::post('exportExcel',   $controllerName . 'exportExcel')->name('exportExcel')->middleware("can:".$controller.".add");
            Route::post('uploadfileExcel', $controllerName . 'uploadfileExcel')->name('uploadfileExcel')->middleware("can:".$controller.".add");

        });
        $prefix = "partner";
        $controller = "partner";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');

        });
        $prefix = "checkout";
        $controller = "checkout";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
        });
        $prefix = "bill";
        $controller = "bill";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::get('info/{id}', $controllerName . 'info')->name('info')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus/{id}', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
            Route::get('updateOrder', $controllerName . 'updateOrder')->name('updateOrder');
            Route::get('select/{status}', $controllerName . 'select')->name('select');
            Route::post('updateProduct', $controllerName . 'updateProduct')->name('updateProduct');
            Route::get('changeBill/{id}', $controllerName . 'changeBill')->name('changeBill')->middleware("can:".$controller.".edit");
        });
        $prefix = "code";
        $controller = "code";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('trash',   $controllerName . 'trash')->name('trash')->middleware("can:".$controller.".index");
            Route::get('add',   $controllerName . 'add')->name('add')->middleware("can:".$controller.".add");
            Route::post('store', $controllerName . 'store')->name('store')->middleware("can:".$controller.".add");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::post('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('restore', $controllerName . 'restore')->name('restore')->middleware("can:".$controller.".edit");
            Route::get('changeStatus', $controllerName . 'changeStatus')->name('changeStatus')->middleware("can:".$controller.".edit");
            Route::get('restoreID/{id}', $controllerName . 'restoreID')->name('restoreID')->middleware("can:".$controller.".edit");
            Route::get('remove/{id}', $controllerName . 'remove')->name('remove')->middleware("can:".$controller.".delete");
            Route::get('removetrash/{id}', $controllerName . 'removetrash')->name('removetrash')->middleware("can:".$controller.".delete");
            Route::post('removeMulti', $controllerName . 'removeMulti')->name('removeMulti')->middleware("can:".$controller.".delete");
        });
        $prefix = "ship";
        $controller = "ship";
        Route::prefix($prefix)->name($controller . ".")->group(function() use ($controller){
            $controllerName = ucfirst($controller) . "Controller@";
            Route::get('/',  $controllerName . 'index')->name('index')->middleware("can:".$controller.".index");
            Route::get('edit/{id}', $controllerName . 'edit')->name('edit')->middleware("can:".$controller.".edit");
            Route::get('update', $controllerName . 'update')->name('update')->middleware("can:".$controller.".edit");
            Route::get('updateProvince', $controllerName . 'updateProvince')->name('updateProvince')->middleware("can:".$controller.".edit");
            Route::get('updateAll', $controllerName . 'updateAll')->name('updateAll')->middleware("can:".$controller.".edit");
            Route::get('district',$controllerName . 'district')->name('district');
        });
    });
});


Route::get('/login','Admin\LoginController@index')->name('login');
Route::post('/login','Admin\LoginController@login')->name('login');
Route::post('/logout','Admin\LoginController@logout')->name('logout');
Route::get('/register','Admin\LoginController@register')->name('register');
Route::post('/store','Admin\LoginController@store')->name('store');
Route::get('/request','Admin\LoginController@request')->name('request');
Route::get('/reset','Admin\ResetpassController@index')->name('resetpass');
Route::post('/sendmail','Admin\ResetpassController@sendmail')->name('sendmail');
Route::get('/checkmail/{token}','Admin\ResetpassController@checkmail')->name('checkmail');
Route::post('/updatePass','Admin\ResetpassController@updatePass')->name('updatePass');


Route::get('/',  'Home\IndexController@home')->name('home');
Route::get('/tin-tuc.html',  'Home\IndexController@news')->name('news');
Route::post('/tin-tuc.html',  'Home\IndexController@searchFullText')->name('searchFullText');
Route::get('/tin-tuc/{slugNew}.html',  'Home\IndexController@showNews')->name('showNews');
Route::get('/lien-he.html',  'Home\IndexController@contact')->name('contact');
Route::get('/gioi-thieu.html',  'Home\IndexController@introduce')->name('introduce');
Route::post('/lien-he/gui-thong-tin',  'Home\IndexController@store')->name('storeContact');
Route::get('/{slugCategory}.html',  'Home\IndexController@showPage')->name('showPage');
Route::get('/thuc-pham.html',  'Home\IndexController@showProduct')->name('product');
Route::get('/thuc-pham/{slugProduct}.html',  'Home\IndexController@showProductDetail')->name('productDetail');
Route::get('/thuc-pham/{slugProduct}/{detail}.html',  'Home\IndexController@detail')->name('detail');
Route::post('/{slugCategory}/gui-thong-tin',  'Home\IndexController@storeComment')->name('storeComment');
Route::post('/{detail}/them-gio-hang',  'Home\IndexController@addCart')->name('addCart');
Route::get('/them-gio-hang',  'Home\IndexController@addCartQuick')->name('addCartQuick');
Route::get('/xoa-gio-hang',  'Home\IndexController@deleteCart')->name('deleteCart');
Route::get('/xoa-tat-ca-gio-hang',  'Home\IndexController@deleteAllCart')->name('deleteAllCart');
Route::get('/thanh-toan/danh-sach-yeu-thich.html',  'Home\IndexController@showWishList')->name('showWishList');
Route::get('/thanh-toan/gio-hang.html',  'Home\IndexController@showCart')->name('showCart');
Route::get('/thanh-toan/thanh-toan.html',  'Home\IndexController@showCheckout')->name('showCheckout');
Route::get('/danh-sach-yeu-thich',  'Home\IndexController@wishList')->name('wishList');
Route::get('/xoa-danh-sach-yeu-thich',  'Home\IndexController@deleteWishList')->name('deleteWishList');
Route::get('/confirmCart/{id}/{token}','Admin\BillController@confirmCart')->name('confirmCart');
Route::get('/ajax/get-quick-view','Home\IndexController@quickView')->name('quickView');
Route::get('/signUp','Home\IndexController@signUp')->name('signUp');
Route::post('/storeSignUp','Home\IndexController@storeSignUp')->name('storeSignUp');
Route::get('/signIn','Home\IndexController@signIn')->name('signIn');
Route::post('/signIn','Home\IndexController@checkSignUp')->name('checksignIn');
Route::get('/signOut','Home\IndexController@signOut')->name('signOut');
Route::get('/profileUser','Home\IndexController@profileUser')->name('profileUser');
Route::post('/profileUser/{id}','Home\IndexController@storeprofileUser')->name('storeprofileUser');
Route::post('/changePassUser/{id}','Home\IndexController@changePassUser')->name('changePassUser');
Route::get('/signIn/facebook', 'SocialAuthController@redirectToProvider')->name('signInFB');
Route::get('/signIn/facebook/callback', 'SocialAuthController@handleProviderCallback')->name('callbackFB');
Route::get('/signIn/{provider}', 'SocialAuthController@redirectToProvider');
Route::get('/signIn/{provide}/callback', 'SocialAuthController@handleProviderCallback');
Route::get('/quickviewCart','Home\IndexController@quickviewCart')->name('quickviewCart');
Route::get('/code','Home\IndexController@code')->name('code');
Route::get('/district','Home\IndexController@district')->name('district');
Route::get('/ward','Home\IndexController@ward')->name('ward');
Route::get('/forgetPass','Home\IndexController@forgetPass')->name('forgetPass');
Route::post('/checkUsername','Home\IndexController@checkUsername')->name('checkUsername');
Route::get('/confirmRestorePass/{id}/{token}','Home\IndexController@confirmRestorePass')->name('confirmRestorePass');
Route::post('/restorePass','Home\IndexController@restorePass')->name('restorePass');
Route::get('/searchFullText','Home\IndexController@searchFullText')->name('searchFullText');
Route::get('/productAjax','Home\IndexController@productAjax')->name('productAjax');


