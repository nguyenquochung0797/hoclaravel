<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Role extends Model
{
    use SoftDeletes;

    protected $table = "roles";

    public function user(){
        return $this->hasMany(User::class, "role_id", "id");
    }
}
