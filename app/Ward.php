<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $table = "ward";

    public function province(){
        return $this->belongsTo(Province::class,'_province_id','id');
    }
    public function district(){
        return $this->belongsTo(District::class,'_district_id','id');
    }
}
