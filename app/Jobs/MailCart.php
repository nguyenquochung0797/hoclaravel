<?php

namespace App\Jobs;

use App\Bill;
use App\Reset as MainModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class MailCart implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $id;
    private $token;
    private $email;

    public function __construct($id,$email,$token)
    {
        $this->id = $id;
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('default.pages.thong-bao-don-hang', ['id' => $this->id,'token' => $this->token,'email' => $this->email], function ($message) {
            $bill = Bill::find($this->id);
            $message->to($bill->email)->subject('Thông báo đơn hàng');
        });

    }
}
