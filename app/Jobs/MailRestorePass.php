<?php

namespace App\Jobs;

use App\Custommer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class MailRestorePass implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $id;
    private $email;
    private $token;
    public function __construct($id,$email,$token)
    {
        $this->id = $id;
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('default.pages.doi-mat-khau',['id' => $this->id,'token' => $this->token,'email' => $this->email],function ($message){
            $custommer = Custommer::find($this->id);
            $message->to($custommer->email)->subject('Khôi phục mật khẩu');
        });
    }
}
