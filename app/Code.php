<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Code extends Model
{
    use SoftDeletes;
    protected $table = "codes";
    protected $dates = [
        'expiry_date'
    ];

    public function code(){

    }
}
