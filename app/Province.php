<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "province";

    public function districts(){
        return $this->hasMany(District::class,'_province_id','id');
    }
    public function wards(){
        return $this->hasMany(Ward::class,'_province_id','id');
    }
}
