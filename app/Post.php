<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = "posts";
    /**
     * The columns of the full text index
     */
    protected $searchable = [
        'name'
    ];


    public function user(){
        return $this->belongsTo(User::class,'user_create','id');
    }

}
