<?php

namespace App\Http\Helper;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Common
{
    public static function uploadImage($file,$folderUpload, $configImage = []){
        $name = time() . '.' . $file->getClientOriginalExtension();
        $img = Image::make($file->getRealPath());
        $img->stream();
        Storage::put($folderUpload . "/" . $name,$img);

        // resize
        foreach($configImage as $folder => $config){
            $img = Image::make($file->getRealPath());
            $img->resize($config['width'],$config['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();
            Storage::put($folderUpload. "/" . $folder . '/' . $name,$img);
        }

        return $name;
    }

    public static function createThead($label, $name, $params){
        $attr = '';
        if($params['sortname'] == $name){
            if($params['sortType'] == "desc"){
                $attr .= 'class="sorting_desc"';
            }else{
                $attr .= 'class="sorting_asc"';
            }
        }else{
            $attr .= 'class="sorting"';
        }
        $html = '<th onclick="sorting(\''.$name.'\', \''.$params['sortType'].'\')" '.$attr.'>'.$label.'</th>';
        return $html;
    }
    public static function pagination($table,$limit){
        $current = $_GET['page']??1;
        $page_current = (int)$current;
        $total_page = ceil(count($table->get())/$limit);
        $page_current = ($page_current>$total_page)?$total_page:(($page_current<1)?1:$page_current);
        $start = ($page_current-1)*$limit;
        return $table->offset($start)->limit($limit);
    }

}
