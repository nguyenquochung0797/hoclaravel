<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class loginCustommer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('loginSuccess')) {
            return view('default.pages.thanh-toan');
        }
        return redirect('signIn');
    }
}
