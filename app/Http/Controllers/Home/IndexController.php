<?php

namespace App\Http\Controllers\Home;

use App\Category;
use App\Code;
use App\Comment;
use App\Custommer;
use App\DetailBill;
use App\District;
use App\Index as MainModel;
use App\Info;
use App\Jobs\MailRestorePass;
use App\Jobs\SendMailJob;
use App\Product;
use App\RestorePassCustommer;
use App\User;
use App\Ward;
use App\Banner;
use App\Post;
use App\Contact;
use Darryldecode\Cart\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Toastr;
use Illuminate\Support\Str;

class IndexController extends Controller
{
    private $viewPath = "default.pages.";

    public function home(Request $request)
    {
        $banner = Banner::orderBy('order', 'desc')->limit(5)->get();
        $post = Post::where('status', 1)->orderBy('order', 'desc')->limit(5)->get();
        $comment = Comment::where('status', 1)->get();
        $data['comments'] = $comment;
        $data['posts'] = $post;
        $data['banners'] = $banner;
        return view($this->viewPath . 'index')->with($data);
    }

    public function news()
    {
        $query = $_GET['search'] ?? '';
        $posts = Post::where('status', 1);
        if ($query != null) {
            $posts->where('name', 'like', '%' . $query . '%');
        }
        $current = $_GET['page'] ?? 1;
        $page_current = (int)$current;
        $limit = 2;
        $total_page = ceil(count($posts->get()) / $limit);
        $page_current = ($page_current > $total_page) ? $total_page : (($page_current < 1) ? 1 : $page_current);
        $start = ($page_current - 1) * $limit;
        $mainModel = $posts->offset($start)->limit($limit)->get();
        $data['page_current'] = $page_current;
        $data['total_page'] = $total_page;
        $data['posts_news'] = $mainModel;
        return view($this->viewPath . 'tin-tuc')->with($data);
    }

    public function searchFullText(Request $request)
    {
        if ($request->search != '') {
            $post = Post::where('status', 1)->where('name', 'like', '%' . $request->search . '%')->get();
        }
        $data['post']=$post;
        return view($this->viewPath.'search')->with($data);
    }

    public function signUp()
    {
        return view($this->viewPath . 'signUp');
    }

    public function signIn()
    {
        return view($this->viewPath . 'signIn');
    }

    public function signOut()
    {
        Session::forget('loginSuccess');
        Session::forget('IDUser');
        return redirect()->back();
    }

    public function storeprofileUser(Request $request,$id){
        $custommer = Custommer::find($request->id);
        $custommer->name = $request->name;
        $custommer->email =  $request->email;
        $custommer->phone =  $request->phone;
        $custommer->address =  $request->address;
        $custommer->picture =  $request->picture_url;
        $custommer->birday =  $request->date;
        $custommer->sex =  $request->gt;
        $custommer->save();
        Toastr::success('Lưu thông tin thành công', 'Thành công');
        return redirect()->back();
    }

    public function changePassUser(Request $request,$id){
        $custommer = Custommer::find($request->id);
        if(Hash::check($request->password_old,$custommer->password)){
            $custommer->password = Hash::make($request->password_new);
            $custommer->save();
            Toastr::success('Đổi mật khẩu thành công', 'Thành công');
            return redirect()->back();
        }
        Toastr::error('Đổi mật khẩu thất bại', 'Thât bại');
        return redirect()->back();
    }

    public function checkSignUp(Request $request)
    {
        $value = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $custommer = Custommer::where('status', 1)->where($value, $request->username)->first();

        if (Hash::check($request->password, $custommer->password)) {
            Session::put('loginSuccess', $custommer->name);
            Session::put('IDUser', $custommer->id);
            $data['custommer'] = $custommer;
            return view($this->viewPath . 'signInSuccess')->with($data);
        } else {
            session()->flash('checkLogin','Tài khoản hoặc mật khẩu hiện tại không đúng');
            return redirect()->route('signIn');
        }
    }

    public function storeSignUp(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:30',
            'username' => 'required|unique:custommers|min:6|max:30',
            'email' => 'required|unique:custommers|email',
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password|min:6',
            'g-recaptcha-response' => 'required|captcha',
        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'email' => ':attribute không hợp lệ',
            'same' => 'Mật khẩu không giống nhau',
            'unique' => ':attribute đã được đăng kí',
        ], [
            'name' => 'Họ và tên',
            'username' => 'Tài khoản',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'confirmPassword' => 'Mật khẩu',
            'g-recaptcha-response' => 'Xác nhận',
        ], [
        ]);

        $custommer = new Custommer();
        $custommer->name = $request->name;
        $custommer->username = $request->username;
        $custommer->email = $request->email;
        $custommer->picture = '/userfiles/images/custommer/user-custommer.png';
        $custommer->password = Hash::make($request->password);
        $custommer->save();
        Toastr::success('Đăng kí thành công', 'Thành công');
        return redirect()->route('signUp');
    }

    public function showNews($slug)
    {
        $post = Post::where('status', 1)->where('slug', $slug)->first();
        $data['post'] = $post;
        return view($this->viewPath . 'chi-tiet-tin-tuc')->with($data);
    }

    public function contact()
    {
        $info = Info::find(1);
        $data['info'] = $info;
        return view($this->viewPath . 'lien-he')->with($data);
    }

    public function introduce()
    {
        return view($this->viewPath . 'gioi-thieu');
    }

    public function showProductDetail($slug)
    {
        $data['slug'] = Category::where('slug', $slug)->first();
        if (!$data['slug']) {
            abort(404);
        }

        $data['categories'] = Category::where('status', 1)->get();
        $title = Category::where('status', 1)->where('id', $data['slug']->id)->first();
        $products = Product::where('status',1)->where('category_id', $data['slug']->id);

        $current = $_GET['page'] ?? 1;
        $page_current = (int)$current;
        $limit = 6;
        $total_page = ceil(count($products->get()) / $limit);
        $page_current = ($page_current > $total_page) ? $total_page : (($page_current < 1) ? 1 : $page_current);
        $start = ($page_current - 1) * $limit;
        $mainModel = $products->offset($start)->limit($limit)->get();

        $data['slugCategory']=$slug;
        $data['page_current'] = $page_current;
        $data['total_page'] = $total_page;
        $data['products'] = $mainModel;
        $data['title'] = $title;
        return view($this->viewPath . 'san-pham')->with($data);
    }

    public function detail($slugProduct, $detail)
    {
        $product = Product::where('slug', $detail)->first();
        if (!$product) {
            abort(404);
        }
        $category = Category::where('slug', $slugProduct)->first();


        $data['product'] = $product;
        $data['picture_galler'] = json_decode($data['product']->picture_galler);
        $data['category'] = $category;

        return view($this->viewPath . 'chi-tiet-san-pham')->with($data);
    }

    public function showCart(Request $request)
    {
        $category = Category::where('status', 1)->get();
        $items = \Cart::session('bill')->getContent();
        $data['category'] = $category;
        $data['items'] = $items;
        return view($this->viewPath . 'gio-hang')->with($data);
    }

    public function addCart(Request $request, $slug)
    {
        $items = \Cart::session('bill')->getContent();

        $product = Product::where('status', 1)->where('id', $request->id)->orWhere('slug', $slug)->first();
        $request->validate([
            'quantity' => 'required|numeric|between:1,' . $product->quantily,
        ], [
            'required' => ':attribute không được rỗng',
            'numeric' => ':attribute không hợp lệ',
            'between' => ':attribute không đủ',
        ], [
            'quantity' => 'Số lượng',
        ]);
        $v = [];
        foreach ($items as $value) {
            array_push($v, $value->id);
        }
        if (!in_array($product->id, $v)) {
            \Cart::session('bill')->add(array(
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price_saleoff,
                'quantity' => $request->quantity ?? 1,
                'conditions' => [
                    'picture' => $product->picture,
                    'content' => $product->content,
                ],
            ));
        } else {
            \Cart::session('bill')->update($product->id, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
            ));
        }
        Toastr::success('Đã thêm vào giỏ hàng', 'Thêm thành công');
        return redirect()->back();
    }

    public function addCartQuick(Request $request){
        $items = \Cart::session('bill')->getContent();
        $product = Product::where('status', 1)->where('id', $request->id)->first();
        $v = [];
        foreach ($items as $value) {
            array_push($v, $value->id);
        }
        if (!in_array($product->id, $v)) {
            \Cart::session('bill')->add(array(
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price_saleoff,
                'quantity' => $request->quantity ?? 1,
                'conditions' => [
                    'picture' => $product->picture,
                    'content' => $product->content,
                ],
            ));
        } else {
            \Cart::session('bill')->update($product->id, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
            ));
        }
        return \Cart::session('bill')->getContent()->count();
    }

    public function profileUser()
    {
        if(session()->has('loginSuccess')) {
            return view($this->viewPath . 'profileUser');
        }
        return redirect()->route('signIn') ;

    }

    public function showCheckout(Request $request)
    {
        if (Session::has('loginSuccess')) {
            $request->validate([
                'total' => 'required|not_in:0',
            ], [
                'required' => ':attribute không có sản phẩm',
                'not_in' => ':attribute không có sản phẩm',
            ], [
                'total' => 'Giỏ hàng',
            ]);
            foreach ($request->detail as $detial)
                \Cart::session('bill')->update($detial['id_product'], array(
                    'conditions' => [
                        'picture' => $detial["picture_product"],
                        'total' => $detial["total_product"],
                    ],
                ));
            $data['items'] = \Cart::session('bill')->getContent();
            $data['bill'] = $request->all();
            return view($this->viewPath . 'thanh-toan')->with($data);
        } else {
            return redirect('signIn');
        }
    }

    public function wishList(Request $request)
    {
        Session::push('wistList', $request->id);
    }

    public function deleteWishList(Request $request)
    {
        $list = array_diff(session()->get('wistList'), array($request->id));
        Session::forget('wistList');
        foreach ($list as $key => $value) {
            Session::push('wistList', $value);
        }
        return view($this->viewPath . 'xoa-danh-sach-yeu-thich');
    }

    public function seach(Request $request)
    {
        $seach = str_replace(' ', '-', $request->s);
        $post = Post::where('status', 1)->where('name', 'like', '%' . $seach . '%')
            ->orWhere('slug', 'like', '%' . $seach . '%')
            //->orWhere('content','like','%'.$seach.'%')
            ->paginate(6);
        $data['search'] = $post;
        return view($this->viewPath . 'timkiem')->with($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:6|max:500',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'contentContact' => ' required|min:6|max:500',
        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'digits' => ':attribute không đủ 10 số',
            'email' => ':attribute không đúng định dạng',

        ], [
            'name' => 'Tên',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
            'contentContact' => 'Nội dung',
        ]);
        $mainModel = new Contact();
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentContact;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->save();
        Toastr::success('Gửi thành công');
        return redirect()->back();

    }

    public function storeComment(Request $request, $slug)
    {
        $request->validate([
            'name' => 'required|min:6|max:500',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'contentCustommer' => ' required|min:6|max:500',
        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'digits' => ':attribute không đủ 10 số',
            'email' => ':attribute không đúng định dạng',

        ], [
            'name' => 'Tên',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
            'contentCustommer' => 'Nội dung',
        ]);
        $mainModel = new Comment();
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentCustommer;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->picture = $request->picture;
        $mainModel->status = 1;
        $mainModel->save();
        Toastr::success('Gửi thành công');
        return redirect()->back();

    }

    public function quickView(Request $request)
    {
        $product = Product::find($request->id);
        $data['product'] = $product;
        return view($this->viewPath . 'quick_view_modal')->with($data);
    }

    public function quickviewCart(Request $request)
    {
        $detail_bill = DetailBill::where('bill_id',$request->id)->get();
        $data['items'] = $detail_bill;
        return view($this->viewPath . 'quickViewCart')->with($data);
    }

    public function deleteCart(Request $request)
    {
        \Cart::session('bill')->remove($request->id);
        $items = \Cart::session('bill')->getContent();
        $data['items'] = $items;
        return view($this->viewPath.'xoa-gio-hang')->with($data);
    }

    public function deleteAllCart()
    {
        \Cart::session('bill')->clear();
    }

    public function showWishList()
    {
        return view($this->viewPath . 'danh-sach-yeu-thich');
    }

    public function code(Request $request){
        $code= Code::where('code',$request->code)->first();
        if($code->discount_type=='percent'){
            session()->put('code',$request->code);
            return $code->discount_percent;
        }
        session()->put('code',$request->code);
        return $code->discount_VND;
    }

    public function district(Request $request){
        $districts = District::where('_province_id',$request->province)->get();
        $data['districts'] =$districts;
        return view($this->viewPath.'district')->with($data);
    }

    public function ward(Request $request){
        $wards = Ward::where('_district_id',$request->district)->get();
        $data['wards'] =$wards;
        return view($this->viewPath.'ward')->with($data);
    }

    public function forgetPass(){
        return view($this->viewPath.'forgetPass');
    }

    public function checkUsername (Request $request){
        $request->validate(
            [
                'username' => 'required',
                'email' => 'required'
            ],
            [
                'required' => ':attribute không được rỗng'
            ],
            [
                'username' => 'Tài khoản',
                'email' => 'Email'
            ]
        );
        $custommer = Custommer::where('username',$request->username);
        if(count($custommer->get())==0){
            Toastr::error('Tài khoản không tồn tại', 'Thất bại');
            return redirect()->back();
        }
        $emailCheck = $custommer->where('email',$request->email);
        if(count($emailCheck->get())==0){
            Toastr::error('Email không tồn tại', 'Thất bại');
            return redirect()->back();
        }

        $token = Str::random(60);
        $email = $request->email;
        $check = $custommer->first();
        $id = $check->id;
        $restorePass = new RestorePassCustommer();
        $restorePass->email = $email;
        $restorePass->token = $token;
        $restorePass->save();
        MailRestorePass::dispatch($id,$email,$token);
        Toastr::warning('Đã gửi email', 'Thành công');
        return redirect()->back();
    }

    public function confirmRestorePass($id,$token){
        $checkUser = RestorePassCustommer::where('token',$token)->first();
        if(Carbon::parse($checkUser->updated_at)->addMinute(1)->isPast()){
            RestorePassCustommer::where('token', $token)->delete();
            return view($this->viewPath.'xac-nhan-mat-khau-that-bai');
        }
        $data['id'] = $id;
        return view($this->viewPath.'restorePass')->with($data);
    }

    public function restorePass(Request $request){
        $request->validate(
            [
                'password' => 'required',
                'password_confirm' => 'required|same:password'
            ],
            [
                'required' => ':attribute không được rỗng',
                'same' => ':attribute không giống'
            ],
            [
                'password' => 'Mật khẩu',
                'password_confirm' => 'Mật khẩu'
            ]);
        $custommer = Custommer::find($request->id);
        $custommer->password = Hash::make($request->password);
        $custommer->save();
        Toastr::success('Đã mật khẩu', 'Thành công');
        return redirect()->route('signIn');
    }

    public function productAjax(Request $request){
        $products = Product::where('status',1)->where('category_id',$request->id);

        $current = $_GET['page'] ?? 1;
        $page_current = (int)$current;
        $limit = 6;
        $total_page = ceil(count($products->get()) / $limit);
        $page_current = ($page_current > $total_page) ? $total_page : (($page_current < 1) ? 1 : $page_current);
        $start = ($page_current - 1) * $limit;
        $mainModel = $products->offset($start)->limit($limit)->get();
        $slug = Category::find($request->id);

        $data['slugCategory']=$slug->slug;
        $data['page_current'] = $page_current;
        $data['total_page'] = $total_page;
        $data['products']=$mainModel;
        return view($this->viewPath.'san-pham-ajax')->with($data);
    }
}
