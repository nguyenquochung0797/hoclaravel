<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use App\Info as MainModel;
use Toastr;



class InfoController extends Controller
{
    private $viewPath = "admin.pages.info.";

    private $acceptedSort = [
        'id', 'name','status','order','created_at'
    ];

    public function index(Request $request){
        $mainModel = MainModel::select("*")->first();
        $data['items'] = $mainModel;
        return view($this->viewPath . 'edit')->with($data);
    }

    public function update(Request $request){
        $request->validate([
            'name' => 'required|min:6|max:500',
            'email' => 'required|min:6|max:500',
            'phone' => 'required|min:6|max:500',
            'address' => 'required|min:6|max:500',
            'web' => 'required|min:6|max:500',
            'time' => 'required|min:6|max:500',
            'contentCty' => 'required|min:6|max:500',
            'picture_url' =>'required'
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
        ],[
            'name' => 'Tên công ty',
            'email' => 'Email',
            'phone' => 'Số điện thoại',
            'address' => 'Địa chỉ',
            'web' => 'Website',
            'time' => 'Thời giam làm việc',
            'contentCty' => 'Nội dụng',
            'picture_url' => 'Hình ảnh'
        ]);
        $mainModel = MainModel::find(1);
        $mainModel->name = $request->name;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->address = $request->address;
        $mainModel->web = $request->web;
        $mainModel->time = $request->time;
        $mainModel->content = $request->contentCty;
        $mainModel->picture = $request->picture_url;

        $mainModel->email2 = $request->email2;


        $mainModel->phone2 = $request->phone2;
        $mainModel->phone3 = $request->phone3;

        $mainModel->pictureView = $request->picture_url2;
        $mainModel->pictureAbout = $request->picture_url3;
        $mainModel->pictureContact = $request->picture_url4;
        $mainModel->pictureNews = $request->picture_url5;

        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }



}
