<?php

namespace App\Http\Controllers\Admin;

use App\Banner as MainModel;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Toastr;

class BannerController extends Controller
{
    private $table = 'banner';
    private $testGit = 'test' ;
    private $viewPath = "admin.pages.banner.";

    private $acceptedSort = [
        'id', 'picture','status','order','created_at'
    ];


    public function index(Request $request){
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_status : -1;
        $params['fillter_order'] = isset($request->fillter_order) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_order : -1;
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if($params['fillter_status'] != -1){
            $mainModel->where('status', $params['fillter_status']);
        }

        if($params['fillter_order'] == 0) {
            $mainModel->orderBy('order', 'desc');
        }else if($params['fillter_order'] == 1){
            $mainModel->orderBy('order', 'asc');
        }

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel ->paginate(6);

        $data['title'] = $this->table;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function add(Request $request){
        return view($this->viewPath . 'add');
    }
    public function store(Request $request){
        // 1 su dung lop request
        // hoac su dung lop validator cua laravel
        // tao request moi rieng biet

        $request->validate([
            'status' => 'required|in:0,1',
            'picture_url' => 'required',
            'link' => 'required',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng'
        ],[
            'status' => 'Trạng thái',
            'link' => 'Link',
            'picture_url' => 'Hình ảnh'
        ]);


        $mainModel = new MainModel();
        $mainModel->status = $request->status;
        $mainModel->picture = $request->picture_url;
        $mainModel->link = $request->link;;
        $mainModel->save();
        Toastr::success('Đã thêm', 'Thành công');
        return redirect()->back();
    }
    public function edit($id){
        $mainModel = MainModel::find($id);
        $data['items']=$mainModel;
        return view($this->viewPath .'edit')->with($data);
    }
    public function update(Request $request){
        $request->validate([
            'status' => 'required|in:0,1',
            'picture_url' => 'required',
            'link' => 'required',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
        ],[
            'status' => 'Trạng thái',
            'link' => 'Link',
            'picture_url' => 'Hình ảnh'
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->status = $request->status;
        $mainModel->picture = $request->picture_url;
        $mainModel->link = $request->link;;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }
    public function remove(Request $request){
        $mainModel = MainModel::find($request->id)->delete();
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function removeMulti(Request $request){
        if($request->cid && count($request->cid) > 0){
            foreach($request->cid as $id){
                MainModel::find($id)->delete();
            }
        }
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function updateOrder(Request $request){
        $mainModel = MainModel::find($request->id);
        $mainModel->order = $request->value;
        $mainModel->save();
    }
    public function restore(Request $request){
        $mainModel =  MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function changeStatus(Request $request){
        $mainModel = MainModel::find($request->id);
        $change = ($mainModel->status==1)?$mainModel->status =0:$mainModel->status =1;
        $mainModel->save();
        $data['title'] = $this->table;
        $data['item']=$mainModel;
        return view($this->viewPath .'changeStatus')->with($data);
    }
    public function trash(){
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel ->paginate(6);
        $data['items']=$mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }
    public function restoreID(Request $request){
        $mainModel =  MainModel::onlyTrashed()->where('id', $request->id);
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function removetrash(Request $request){
        $mainModel = MainModel::onlyTrashed()->find($request->id);
        $mainModel->tag()->detach($request->tag);
        $mainModel->forceDelete();
        return redirect()->back();
    }
}
