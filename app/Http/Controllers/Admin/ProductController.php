<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Exports\ProductsExport;
use App\Imports\ProductsImport;
use App\Jobs\ImportExcel;
use App\Product as MainModel;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Toastr;
class ProductController extends Controller
{
    private $table = 'product';
    private $viewPath = "admin.pages.product.";

    private $acceptedSort = [
        'id', 'name', 'status', 'order', 'created_at'
    ];


    public function index(Request $request)
    {
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [0, 1]) ? $request->fillter_status : -1;
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $params['fillter_order'] = isset($request->fillter_order) && in_array($request->fillter_status, [0, 1]) ? $request->fillter_order : -1;
        $params['fillter_category'] = isset($request->fillter_category) ? $request->fillter_category : '';
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if ($params['fillter_status'] != -1) {
            $mainModel->where('status', $params['fillter_status']);
        }
        if ($query != null) {
            $mainModel->where('name', 'like', '%' . $query . '%')
                ->orWhere('slug', 'like', '%' . $query . '%')
                ->orWhere('status', 'like', $query);
        }
        if ($params['fillter_category'] != null) {
            $mainModel->where('category_id', 'like', '%' . $params['fillter_category'] . '%');
        }
        if ($params['fillter_order'] == 0) {
            $mainModel->orderBy('order', 'desc');
        } else if ($params['fillter_order'] == 1) {
            $mainModel->orderBy('order', 'asc');
        }
        $subModel = Category::select('id', 'name')->get();

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel->paginate(6);
        $subModelUser = User::select('id', 'name')->get();
        $data['title'] = $this->table;
        $data['subModelUser'] = $subModelUser;
        $data['subModel'] = $subModel;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }

    public function add(Request $request)
    {
        $subModel = Category::select('id', 'name')->get();
        $data['subModel'] = $subModel;
        return view($this->viewPath . 'add')->with($data);
    }

    public function store(Request $request)
    {
        // 1 su dung lop request
        // hoac su dung lop validator cua laravel
        // tao request moi rieng biet

        $request->validate([
            'name' => 'required|min:3|max:500',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'quantily' => 'required|numeric',
            'status' => 'required|in:0,1',
            'classify' => 'required|in:0,1,2',
            'category_id' => 'required|not_in:-1',
            'picture_url' => 'required',
            'slug' => 'required|unique:products,slug',
            'contentProduct' => 'required|min:3',

        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng',
            'not_in' => ':attribute không hợp lệ',
            'numeric' => ':attribute không hợp lệ',
        ], [
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'price' => 'Giá',
            'discount' => 'Giá giảm',
            'quantily' => 'Số lượng',
            'contentProduct' => 'Nội dung',
            'category_id' => 'Danh mục',
            'picture_url' => 'Hình ảnh',
            'classify' => 'Phân loại',

        ]);


        $mainModel = new MainModel();
        $mainModel->name = $request->name;
        $mainModel->price = $request->price;
        $mainModel->discount = $request->discount;
        $mainModel->price_saleoff =$request->price-($request->price*$request->discount/100);
        $mainModel->quantily = $request->quantily;
        $mainModel->content = $request->contentProduct;
        $mainModel->category_id = $request->category_id;
        $mainModel->status = $request->status;
        $mainModel->classify = $request->classify;
        $mainModel->picture = $request->picture_url;
        $mainModel->picture_galler = json_encode($request->picture_galler);
        $mainModel->slug = $request->slug;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->user_created = Auth::user()->id;
        $mainModel->save();
        Toastr::success('Đã thêm', 'Thành công');
        return redirect()->back();
    }

    public function edit($id)
    {
        $mainModel = MainModel::find($id);
        $data['picture_galler'] = json_decode($mainModel->picture_galler);
        $subModel = Category::select('id', 'name')->get();
        $data['subModel'] = $subModel;
        $data['items'] = $mainModel;
        return view($this->viewPath . 'edit')->with($data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:500',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'quantily' => 'required|numeric',
            'status' => 'required|in:0,1',
            'classify' => 'required|in:0,1,2',
            'category_id' => 'required|not_in:-1',
            'picture_url' => 'required',
            'slug' => 'required|unique:products,slug,' . $request->id,
            'contentProduct' => 'required|min:3',
        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng',
            'not_in' => ':attribute không hợp lệ',
            'numeric' => ':attribute không hợp lệ',
        ], [
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'price' => 'Giá',
            'discount' => 'Giá giảm',
            'quantily' => 'Số lượng',
            'contentProduct' => 'Nội dung',
            'category_id' => 'Danh mục',
            'picture_url' => 'Hình ảnh',
            'classify' => 'Phân loại',
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->price = $request->price;
        $mainModel->discount = $request->discount;
        $mainModel->price_saleoff =$request->price-($request->price*$request->discount/100);
        $mainModel->quantily = $request->quantily;
        $mainModel->content = $request->contentProduct;
        $mainModel->category_id = $request->category_id;
        $mainModel->status = $request->status;
        $mainModel->classify = $request->classify;
        $mainModel->picture = $request->picture_url;
        $mainModel->picture_galler = json_encode($request->picture_galler);
        $mainModel->slug = $request->slug;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->user_updated = Auth::user()->id;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }

    public function remove(Request $request)
    {
        $mainModel = MainModel::find($request->id)->delete();
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }

    public function removeMulti(Request $request)
    {
        if ($request->cid && count($request->cid) > 0) {
            foreach ($request->cid as $id) {
                MainModel::find($id)->delete();
            }
        }
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }

    public function updateOrder(Request $request){
        $mainModel = MainModel::find($request->id);
        $mainModel->order = $request->value;
        $mainModel->save();
    }

    public function updatePrice(Request $request)
    {
        $mainModel = MainModel::find($request->id);
        $mainModel->price = $request->value;
        $mainModel->save();
    }

    public function updateDiscount(Request $request)
    {
        $mainModel = MainModel::find($request->id);
        $mainModel->discount = $request->value;
        $mainModel->save();
    }

    public function restore(Request $request)
    {
        $mainModel = MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if ($mainModel) {
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function changeStatus(Request $request){
        $mainModel = MainModel::find($request->id);
        $change = ($mainModel->status==1)?$mainModel->status =0:$mainModel->status =1;
        $mainModel->save();
        $data['title'] = $this->table;
        $data['item']=$mainModel;
        return view($this->viewPath .'changeStatus')->with($data);
    }

    public function trash()
    {
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel->paginate(6);
        $data['items'] = $mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }

    public function restoreID(Request $request)
    {
        $mainModel = MainModel::onlyTrashed()->where('id', $request->id);
        if ($mainModel) {
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function removetrash(Request $request)
    {
        $mainModel = MainModel::onlyTrashed()->find($request->id);
        $mainModel->tag()->detach($request->tag);
        $mainModel->forceDelete();
        return redirect()->back();
    }

    public function importExcel (){
        $files = Storage::files('upload');
        //dd($files);
        $data['files'] = $files;
        return view($this->viewPath.'importExcel')->with($data);
    }

    public function uploadfileExcel(Request $request){
        $file = $request->file('name');
        Storage::putFileAs( "upload/",$request->file('name'),$file->getClientOriginalName());
        return redirect()->back();
    }

    public function storefromExcel(Request $request){
        $file_name = $request->file_name;
        ImportExcel::dispatch($file_name);
        return redirect()->back();
    }

    public function exportExcel(Request $request){
        return Excel::download(new ProductsExport, 'products.'.$request->get('format'));
    }

}
