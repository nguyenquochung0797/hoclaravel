<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use App\Contact as MainModel;
use Toastr;



class ContactController extends Controller
{
    private $table = 'contact';
    private $viewPath = "admin.pages.contact.";

    private $acceptedSort = [
        'id', 'name','status','order','created_at'
    ];

    public function index(Request $request){
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_status : -1;
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if($params['fillter_status'] != -1){
            $mainModel->where('status', $params['fillter_status']);
        }

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel ->paginate(6);

        $data['title'] = $this->table;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function add(Request $request){
        $data = [];
        return view($this->viewPath . 'add')->with($data);
    }
    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:6|max:500',
            'email' => 'required|unique:users',
            'phone' => 'required',
            'contentContact' =>' required|min:6|max:500',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
        ],[
            'name' => 'Tên',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
            'contentContact' => 'Nội dung',
        ]);
        $mainModel = new MainModel();
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentContact;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->save();
        Toastr::success('Đã thêm', 'Thành công');
        return redirect()->back();
    }
    public function edit($id){
        $mainModel = MainModel::find($id);
        $data['items']=$mainModel;
        return view($this->viewPath .'edit')->with($data);
    }
    public function update(Request $request){;
        $request->validate([
            'name' => 'required|min:6|max:500',
            'email' => 'required',
            'phone' => 'required',
            'contentCustommer' =>' required|min:6|max:500',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
        ],[
            'name' => 'Tên',
            'phone' => 'Số điện thoại',
            'email' => 'Email',
            'contentCustommer' => 'Nội dung',
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentCustommer;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }
    public function remove(Request $request){
        $mainModel = MainModel::find($request->id)->delete();
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function removeMulti(Request $request){
        if($request->cid && count($request->cid) > 0){
            foreach($request->cid as $id){
                MainModel::find($id)->delete();
            }
        }
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function restore(Request $request){
        $mainModel =  MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function trash(){
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel ->paginate(6);
        $data['items']=$mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }
    public function restoreID(Request $request){
        $mainModel =  MainModel::onlyTrashed()->where('id', $request->id);
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function removetrash(Request $request){
        $mainModel = MainModel::onlyTrashed()->find($request->id)->forceDelete();
        return redirect()->back();
    }




}
