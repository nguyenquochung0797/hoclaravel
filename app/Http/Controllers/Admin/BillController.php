<?php

namespace App\Http\Controllers\Admin;

use App\Bill as MainModel;
use App\Confirmcart;
use App\DetailBill;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Jobs\MailCart;
use Illuminate\Support\Str;
use Toastr;


class BillController extends Controller
{
    private $table = 'bill';
    private $viewPath = "admin.pages.bill.";
    private $viewPathHome = "default.pages.";

    private $acceptedSort = [
        'id', 'name', 'status', 'order', 'created_at'
    ];


    public function index(Request $request)
    {
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [0, 1, 2, 3]) ? $request->fillter_status : -1;
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $params['fillter_order'] = isset($request->fillter_order) && in_array($request->fillter_status, [0, 1]) ? $request->fillter_order : -1;
        $params['fillter_category'] = isset($request->fillter_category) ? $request->fillter_category : '';
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if ($params['fillter_status'] != -1) {
            $mainModel->where('status', $params['fillter_status']);
        }
        if ($query != null) {
            $mainModel->where('name', 'like', '%' . $query . '%')
                ->orWhere('phone', 'like', '%' . $query . '%')
                ->orWhere('status', 'like', $query);
        }

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel->paginate(6);
        $data['title'] = $this->table;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:6|max:30',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'address' => 'required|min:6|max:100',
            'province' => 'required',
            'district' => 'required',
            'ward' => 'required',
        ], [
            'required' => ':attribute không có sản phẩm',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'email' => ':attribute không hợp lệ',
            'digits' => ':attribute không đủ 10 số',
        ], [
            'name' => 'Tên',
            'email' => 'Email',
            'phone' => 'Số điện thoại',
            'address' => 'Địa chỉ',
            'province' => 'Thành phố/Tỉnh',
            'district' => 'Quận/Huyện',
            'ward' => 'Phường/Xã',
        ]);
        $mainModel = new MainModel();
        $mainModel->status = 0;
        $mainModel->name = $request->name;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->address = $request->address;
        $mainModel->totalBill = $request->totalBill;
        $mainModel->custommer_id = $request->id;
        $mainModel->save();

        $items = \Cart::session('bill')->getContent();

        foreach ($items as $item) {
            $modelDetailBill = new DetailBill();
            $modelDetailBill->name = $item->name;
            $modelDetailBill->picture = $item->conditions['picture'];
            $modelDetailBill->price = $item->price;
            $modelDetailBill->quantity = $item->quantity;
            $modelDetailBill->total = $item->conditions['total'];
            $modelDetailBill->bill_id = $mainModel->id;
            $modelDetailBill->save();
            $product = Product::find($item->id);
            $product->quantily = $product->quantily - $item->quantity;
            $product->save();
        }
        $token = Str::random(60);
        $confirmModel = new Confirmcart();
        $confirmModel->email = $mainModel->email;
        $confirmModel->token = $token;
        $confirmModel->save();
        MailCart::dispatch($mainModel->id, $mainModel->email, $token);

        \Cart::session('bill')->clear();
        session()->forget('code');
        Toastr::success('Đã đặt hàng thành công', 'Thành công');
        return view($this->viewPathHome . 'gio-hang');
    }

    public function confirmCart(Request $request, $id, $token)
    {
        $bill = MainModel::find($id);
        if($bill->status != 3){
        $tokenModel = Confirmcart::where('token', $token)->first();
            if (Carbon::parse($tokenModel->updated_at)->addMinutes(1)->isPast()) {
                Confirmcart::where('token', $token)->delete();
                $bill = MainModel::find($id);
                $bill->status = 3;
                $bill->save();
                return view($this->viewPathHome . 'don-hang-het-han');
            }
            if ($tokenModel->email == $request->email) {
                Confirmcart::where('token', $token)->delete();
                $bill = MainModel::find($id);
                $bill->status = 1;
                $bill->save();
                return view($this->viewPathHome . 'xac-nhan-don-hang');
            }
        }
        return view($this->viewPathHome . 'xac-nhan-don-hang-that-bai');
    }

    public function info($id)
    {
        $mainModel = MainModel::find($id);
        $subModel = DetailBill::where('bill_id', $mainModel->id)->get();
        $data['items'] = $mainModel;
        $data['detail'] = $subModel;
        return view($this->viewPath . 'infoBill')->with($data);
    }

    public function changeBill($id)
    {
        $mainModel = MainModel::find($id);
        $mainModel->status = 3;
        $mainModel->save();
        Toastr::warning('Đã huỷ đơn hàng', 'Huỷ đơn');
        return redirect()->back();
    }


    public function edit($id)
    {
        $mainModel = MainModel::find($id);
        $subModel = DetailBill::where('bill_id', $mainModel->id)->get();
        $data['items'] = $mainModel;
        $data['detail'] = $subModel;
        return view($this->viewPath . 'edit')->with($data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|min:6|max:50',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'address' => 'required|min:6|max:500',
        ], [
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'email' => ':attribute không hợp lệ',
            'digits' => ':attribute không đủ 10 số',
        ], [
            'name' => 'Tên',
            'email' => 'Email',
            'phone' => 'Số điện thoại',
            'address' => 'Địa chỉ',
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->address = $request->address;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }

    //---------------------------------------------------------
    public function remove(Request $request)
    {
        $mainModel = MainModel::find($request->id)->delete();;
        return redirect()->back();
    }

    public function removeMulti(Request $request)
    {
        if ($request->cid && count($request->cid) > 0) {
            foreach ($request->cid as $id) {
                MainModel::find($id)->delete();
            }
        }
        return redirect()->back();
    }

    public function updateOrder(Request $request)
    {
        foreach ($request->order as $id => $v) {
            $mainModel = MainModel::find($id);
            $mainModel->order = $v;
            $mainModel->save();
        }
        return redirect()->back();
    }

    public function restore(Request $request)
    {
        $mainModel = MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if ($mainModel) {
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function changeStatus(Request $request)
    {
        $mainModel = MainModel::find($request->id);
        if ($mainModel->status == 1 || $mainModel->status == 2) {
            $change = ($mainModel->status == 1) ? $mainModel->status = 2 : $mainModel->status = 1;
            $mainModel->save();
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function trash()
    {
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel->paginate(6);
        $data['items'] = $mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }

    public function restoreID(Request $request)
    {
        $mainModel = MainModel::onlyTrashed()->where('id', $request->id);
        if ($mainModel) {
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function removetrash(Request $request)
    {
        $subModel = DetailBill::where('bill_id', $request->id)->get();
        foreach ($subModel as $item) {
            $item->forceDelete();
        }
        $mainModel = MainModel::onlyTrashed()->find($request->id);
        $mainModel->forceDelete();
        return redirect()->back();
    }


}
