<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Tag;
use App\User;
use App\Post as MainModel;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Toastr;

class PostController extends Controller
{
    private $table = 'post';
    private $viewPath = "admin.pages.post.";

    private $acceptedSort = [
        'id', 'name','status','order','created_at'
    ];
    private $folderUpload = "post";


    public function index(Request $request){
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_status : -1;
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $params['fillter_order'] = isset($request->fillter_order) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_order : -1;
        $params['fillter_category'] = isset($request->fillter_category)  ? $request->fillter_category : '';
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if($params['fillter_status'] != -1){
            $mainModel->where('status', $params['fillter_status']);
        }
        if($query != null){
            $mainModel->where('name','like','%'.$query.'%')
                ->orWhere('slug','like','%'.$query.'%')
                ->orWhere('status','like',$query);
        }
        if($params['fillter_category'] != null){
            $mainModel->where('category_id','like','%'.$params['fillter_category'].'%');
        }
        if($params['fillter_order'] == 0) {
            $mainModel->orderBy('order', 'desc');
        }else if($params['fillter_order'] == 1){
            $mainModel->orderBy('order', 'asc');
        }
        $subModel = Category::select('id','name')->get();

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel ->paginate(6);
        $subModelUser = User::select('id','name')->get();
        $data['title'] = $this->table;
        $data['subModelUser']= $subModelUser;
        $data['subModel']= $subModel;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function add(Request $request){
        $subModel = Category::select('id','name')->get();
        $data['subModel']= $subModel;
        return view($this->viewPath . 'add')->with($data);
    }
    public function store(Request $request){
        // 1 su dung lop request
        // hoac su dung lop validator cua laravel
        // tao request moi rieng biet

        $request->validate([
            'name' => 'required|min:6|max:500',
            'status' => 'required|in:0,1',
            'slug' => 'required|unique:posts,slug',
            'contentPost' => 'required|min:6',
            'picture_url' => 'required',

        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng',
            'not_in' => ':attribute không hợp lệ',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'contentPost'=> 'Nội dung',
            'category_id'=> 'Danh mục',
            'picture_url' => 'Hình ảnh',
        ]);


        $mainModel = new MainModel();
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentPost;
        $mainModel->status = $request->status;
        $mainModel->picture = $request->picture_url;
        $mainModel->slug = $request->slug;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->user_create = Auth::user()->id;
        $mainModel->save();
        Toastr::success('Đã thêm', 'Thành công');
        return redirect()->back();
    }
    public function edit($id){
        $mainModel = MainModel::find($id);
        $subModel = Category::select('id','name')->get();
        $data['subModel']= $subModel;
        $data['items']=$mainModel;
        return view($this->viewPath .'edit')->with($data);
    }
    public function update(Request $request){
        $request->validate([
            'name' => 'required|min:6|max:500',
            'status' => 'required|in:0,1',
            'slug' => 'required|unique:posts,slug,' . $request->id,
            'contentPost' => 'required|min:6',
            'picture_url' => 'required',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'not_in' => ':attribute không hợp lệ',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'contentPost'=> 'Nội dung',
            'category_id'=> 'Danh mục',
            'picture_url' => 'Hình ảnh',
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->content = $request->contentPost;
        $mainModel->status = $request->status;
        $mainModel->slug = $request->slug;
        $mainModel->picture = $request->picture_url;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->user_update = Auth::user()->id;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }
    public function remove(Request $request){
        $mainModel = MainModel::find($request->id)->delete();
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function removeMulti(Request $request){
        if($request->cid && count($request->cid) > 0){
            foreach($request->cid as $id){
                MainModel::find($id)->delete();
            }
        }
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function updateOrder(Request $request){
        $mainModel = MainModel::find($request->id);
        $mainModel->order = $request->value;
        $mainModel->user_update = Auth::user()->id;
        $mainModel->save();
    }
    public function restore(Request $request){
        $mainModel =  MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }

    public function changeStatus(Request $request){
        $mainModel = MainModel::find($request->id);
        $change = ($mainModel->status==1)?$mainModel->status =0:$mainModel->status =1;
        $mainModel->save();
        $data['title'] = $this->table;
        $data['item']=$mainModel;
        return view($this->viewPath .'changeStatus')->with($data);
    }

    public function trash(){
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel ->paginate(6);
        $data['items']=$mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }
    public function restoreID(Request $request){
        $mainModel =  MainModel::onlyTrashed()->where('id', $request->id);
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function removetrash(Request $request){
        $mainModel = MainModel::onlyTrashed()->find($request->id);
        $mainModel->tag()->detach($request->tag);
        $mainModel->forceDelete();
        return redirect()->back();
    }




}
