<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\User as MainModel;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Toastr;



class UserController extends Controller
{
    private $table = 'user';
    private $viewPath = "admin.pages.user.";

    private $acceptedSort = [
        'id', 'name','status','order','created_at'
    ];


    public function index(Request $request){
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_status : -1;
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if($params['fillter_status'] != -1){
            $mainModel->where('status', $params['fillter_status']);
        }
        if($query != null){
            $mainModel->where('name','like','%'.$query.'%')
                ->orWhere('status','like',$query);
        }

        $subModel = Category::select('id','name')->get();
        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel ->paginate(6);


        $data['title'] = $this->table;
        $data['subModel']= $subModel;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function add(Request $request){
        $subModel = Role::select('id','name')->get();
        $data['subModel']= $subModel;
        return view($this->viewPath . 'add')->with($data);
    }
    public function store(Request $request){
        // 1 su dung lop request
        // hoac su dung lop validator cua laravel
        // tao request moi rieng biet
        $request->validate([
            'name' => 'required|min:6|max:500',
            'status' => 'required|in:0,1',
            'role' => 'required|not_in:-1',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'password_confirm' => 'required|same:password',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'not_in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng',
            'same' => ':attribute không giống nhau',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'email' => 'Email',
            'role' => 'Quyền',
            'password'=>'Mật khẩu',
            'password_confirm'=>'Mật khẩu'
        ]);


        $mainModel = new MainModel();
        $mainModel->name = $request->name;
        $mainModel->status = $request->status;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->password = Hash::make($request->password);
        $mainModel->save();
        Toastr::success('Đã thêm', 'Thành công');
        return redirect()->back();
    }
    public function edit($id){
        $mainModel = MainModel::find($id);
        $subModel = Role::select('id','name')->get();
        $data['subModel']= $subModel;
        $data['items']=$mainModel;
        return view($this->viewPath .'edit')->with($data);
    }
    public function update(Request $request){
        $request->validate([
            'name' => 'required|min:6|max:50',
            'status' => 'required|in:0,1',
            'email' => 'required|unique:users,email,' . $request->id,
            'role' => 'required|not_in:-1',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'not_in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'email' => 'Email',
            'role' => 'Quyền',
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->status = $request->status;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->phone;
        $mainModel->role_id= $request->role;
        $mainModel->save();
        Toastr::success('Đã sửa', 'Thành công');
        return redirect()->back();
    }

    public function editPass($id){
        $mainModel = MainModel::find($id);
        $data['items'] = $mainModel;
        return view($this->viewPath .'editPass')->with($data);
    }
    public function changePass(Request $request){
        $request->validate([
            'password' => 'required|min:6',
            'password_confirm' => 'required|same:password',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'same' => ':attribute không giống nhau'
        ],[
            'password'=>'Mật khẩu',
            'password_confirm'=>'Mật khẩu'
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->password = Hash::make($request->password);
        $mainModel->save();
        Toastr::success('Đổi mật khẩu', 'Thành công');
        return redirect()->back();
    }
    public function remove(Request $request){
        $mainModel = MainModel::find($request->id);
        if($mainModel->username != 'quochung' && $mainModel->email !='nguyenquochung0797@gmail.com'){
            $mainModel->delete();
            Toastr::warning('Đã xoá', 'Thành công');
            return redirect()->back();
        }
        return redirect()->back();
    }
    public function removeMulti(Request $request){
        if($request->cid && count($request->cid) > 0){
            foreach($request->cid as $id){
                MainModel::find($id)->delete();
            }
        }
        Toastr::warning('Đã xoá', 'Thành công');
        return redirect()->back();
    }
    public function restore(Request $request){
        $mainModel =  MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function changeStatus(Request $request){
        $mainModel = MainModel::find($request->id);
        $change = ($mainModel->status==1)?$mainModel->status =0:$mainModel->status =1;
        $mainModel->save();
        $data['title'] = $this->table;
        $data['item']=$mainModel;
        return view($this->viewPath .'changeStatus')->with($data);
    }
    public function trash(){
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel ->paginate(6);
        $data['items']=$mainModel;
        $data['title'] = $this->table;
        return view($this->viewPath . 'trash')->with($data);
    }
    public function restoreID(Request $request){
        $mainModel =  MainModel::onlyTrashed()->where('id', $request->id);
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function removetrash(Request $request){
        $mainModel = MainModel::onlyTrashed()->find($request->id)->forceDelete();
        return redirect()->back();
    }




}
