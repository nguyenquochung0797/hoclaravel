<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use App\District ;
use App\Province as MainModel;




class ShipController extends Controller
{
    private $viewPath = "admin.pages.ship.";

    private $acceptedSort = [
        'id', 'name','created_at'
    ];

    public function index(Request $request){
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";
        $mainModel = MainModel::select("*");
        $mainModel = $mainModel->orderBy($params['sortname']);
        $mainModel = $mainModel ->paginate(32);
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function district(Request $request){
        $districts = District::where('_province_id',$request->id)->get();
        $data['items'] = $districts;
        return view($this->viewPath.'district')->with($data);
    }

    public function update(Request $request){
        $district = District::find($request->id);
        $district->priceShip = $request->price;
        $district->save();
        return ('thành công');
       // return session()->flash('success', 'Thành công');
    }
    public function updateProvince(Request $request){
        $districts = District::where('_province_id',$request->id)->get();
        foreach ($districts as $district){
            $district->priceShip = $request->price;
            $district->save();
        }
        $data['items'] = $districts;
        return view($this->viewPath.'district')->with($data);
    }
    public function updateAll(Request $request){
        $districts = District::all();
        foreach ($districts as $district){
            $district->priceShip = $request->price;
            $district->save();
        }
    }







}
