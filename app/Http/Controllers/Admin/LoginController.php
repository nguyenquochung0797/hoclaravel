<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use Mail;
use App\User;




class LoginController extends Controller
{
    private  $view = 'admin.auth.';
    public function index(){
        return view($this->view.'login');
    }
    public function login(Request $request)
    {
        $value = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $data = [
            $value => $request->username,
            'password' => $request->password,
        ];

        if(Auth::attempt($data))
        {
            return redirect()->route('admin.category.index');
        }else{
            return redirect()->route('login');
        }
    }
    public function logout(){
        Auth::logout();
        return view('admin.auth.login');
    }
    public function register(){
        return view('admin.auth.register');
    }
    public function store(Request $request){
        $request->validate([
            'name' => 'required|min:6|max:50',
            'username' => 'required|min:6|max:50',
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng',
            'same' => ':attribute không giống nhau',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'email' => 'Email',
            'username' => 'Tài khoản',
            'password'=>'Mật khẩu',
            'password_confirm'=>'Mật khẩu'
        ]);
        $mainModel = new User();
        $mainModel->name = $request->name;
        $mainModel->username = $request->username;
        $mainModel->email = $request->email;
        $mainModel->phone = $request->tel;
        $mainModel->password = Hash::make($request->password);
        $mainModel->status = 1;
        $mainModel->role_id = 3;
        $mainModel->save();
        return view('admin.auth.login');
    }


}
