<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Jobs\SendEmailJob;
use App\Jobs\SendMailJob;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use Mail;
use App\User;
use App\Sendmailcart as MainModel;


class SendmailcartController extends Controller
{
    public function index(){
        return view('admin.auth.sendmail');
    }
    public function sendmail(Request $request){
        $mainModel = new MainModel();
        $mainModel->email = $request->email;
        $mainModel->token = Str::random(60);
        $mainModel->save();
        $token = $mainModel->token;
        $email =  $request->email;
        SendMailJob::dispatch($email, $token);
       return redirect()->back();
    }
    public function checkmail(Request $request, $token){
        $mainModel = MainModel::where('token',$token)->first();
        if (Carbon::parse($mainModel->updated_at)->addMinutes(1)->isPast()) {
            MainModel::where('token',$token)->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 422);
        }
        if($mainModel->email==$request->email){
            $data['token'] =$token;
            $data['email'] = $request->email;
            MainModel::where('token',$token)->delete();
            return view('admin.auth.changepass')->with($data);
        }
    }
    public function updatePass(Request $request){
        $user = User::where('email',$request->email)->first();
        $user->password = Hash::make($request->password);
        $user->remember_token= '';
        $user->save();
        return view('admin.auth.sendmail');
    }

}
