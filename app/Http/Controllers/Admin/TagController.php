<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Tag as MainModel;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TagController extends Controller
{
    private $viewPath = "admin.pages.tag.";

    private $acceptedSort = [
        'id', 'name','status','order','created_at'
    ];



    public function index(Request $request){
        $params['fillter_status'] = isset($request->fillter_status) && in_array($request->fillter_status, [ 0, 1])  ? $request->fillter_status : -1;
        $query = isset($_GET['query']) ? $_GET['query'] : '';
        $params['sortname'] = $request->field && in_array($request->field, $this->acceptedSort) ? $request->field : "id";
        $params['sortType'] = $request->type && in_array($request->type, ['desc', 'asc']) ? $request->type : "desc";

        $mainModel = MainModel::select("*");

        if($params['fillter_status'] != -1){
            $mainModel->where('status', $params['fillter_status']);
        }
        if($query != null){
            $mainModel->where('name','like','%'.$query.'%')
                ->orWhere('slug','like','%'.$query.'%')
                ->orWhere('status','like',$query);
        }

        $subModel = Category::select('id','name')->get();

        $mainModel = $mainModel->orderBy($params['sortname'], $params['sortType']);
        $mainModel = $mainModel ->paginate(6);

        $data['subModel']= $subModel;
        $data['items'] = $mainModel;
        $data['params'] = $params;
        return view($this->viewPath . 'index')->with($data);
    }
    public function add(Request $request){
        $subModel = Category::select('id','name')->get();
        $data['subModel']= $subModel;
        return view($this->viewPath . 'add')->with($data);
    }
    public function store(Request $request){
        // 1 su dung lop request
        // hoac su dung lop validator cua laravel
        // tao request moi rieng biet
        $request->validate([
            'name' => 'required|min:3|max:500',
            'status' => 'required|in:0,1',
            'slug' => 'required|unique:tags,slug',
            'picture_url' => 'required'
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
            'mimes' => ':attribute không đúng định dạng'
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'picture_url' => 'Hình ảnh'
        ]);

        $mainModel = new MainModel();
        $mainModel->name = $request->name;
        $mainModel->status = $request->status;
        $mainModel->picture = $request->picture_url;
        $mainModel->slug = $request->slug;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->save();
        session()->flash('success', 'Thành công');
        return redirect()->back();
    }
    public function edit($id){
        $mainModel = MainModel::find($id);
        $subModel = Category::select('id','name')->get();
        $data['subModel']= $subModel;
        $data['items']=$mainModel;
        return view($this->viewPath .'edit')->with($data);
    }
    public function update(Request $request){

        $request->validate([
            'name' => 'required|min:6|max:50',
            'status' => 'required|in:0,1',
            'slug' => 'required|unique:tags,slug,' . $request->id,
            'picture_url' => 'required'
        ],[
            'required' => ':attribute không được rỗng',
            'min' => ':attribute ít nhất :min ký tự',
            'max' => ':attribute không vượt quá :max ký tự',
            'in' => ':attribute không hợp lệ',
            'unique' => ':attribute đã tồn tại',
        ],[
            'name' => 'Tên',
            'status' => 'Trạng thái',
            'slug' => 'Slug',
            'picture_url' => 'Hình ảnh'
        ]);

        $mainModel = MainModel::find($request->id);
        $mainModel->name = $request->name;
        $mainModel->status = $request->status;
        $mainModel->slug = $request->slug;
        $mainModel->picture = $request->picture_url;
        $mainModel->meta_title = $request->meta_title;
        $mainModel->meta_keywords = $request->meta_keywords;
        $mainModel->meta_description = $request->meta_description;
        $mainModel->save();
        session()->flash('success', 'Thành công');
        return redirect()->back();
    }
    public function remove(Request $request){
        $mainModel = MainModel::find($request->id)->delete();;
        return redirect()->back();
    }
    public function removeMulti(Request $request){
        if($request->cid && count($request->cid) > 0){
            foreach($request->cid as $id){
                MainModel::find($id)->delete();
            }
        }
        return redirect()->back();
    }
    public function updateOrder(Request $request){
        foreach($request->order as $id => $v) {
            $mainModel = MainModel::find($id);
            $mainModel->order = $v;
            $mainModel->save();
        }
        return redirect()->back();
    }
    public function restore(Request $request){
        $mainModel =  MainModel::onlyTrashed()->orderBy('deleted_at', 'asc')->first();
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function changeStatus(Request $request){
        $mainModel = MainModel::find($request->id);
        $change = ($mainModel->status==1)?$mainModel->status =0:$mainModel->status =1;
        $mainModel->save();
        return redirect()->back();
    }
    public function trash(){
        $mainModel = MainModel::onlyTrashed();
        $mainModel = $mainModel ->paginate(6);
        $data['items']=$mainModel;
        return view($this->viewPath . 'trash')->with($data);
    }
    public function restoreID(Request $request){
        $mainModel =  MainModel::onlyTrashed()->where('id', $request->id);
        if($mainModel){
            $mainModel->restore();
        }
        return redirect()->back();
    }
    public function removetrash(Request $request){
        $mainModel = MainModel::onlyTrashed()->find($request->id)->forceDelete();
        return redirect()->back();
    }




}
