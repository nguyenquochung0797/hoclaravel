<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Product::select('id','name','price','discount','price_saleoff','quantily')->get();
    }
    public function headings() :array {
        return ["STT", "Name", "Price", "Discount", "PriceSaleOff", "Quantily"];
    }

}
