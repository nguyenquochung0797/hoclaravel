<?php

return [
  'category' => [
      'index' => 'Hiển thị danh sách danh mục',
      'add' => 'Thêm mới danh mục',
      'edit' => 'Sửa danh mục',
      'delete' => 'Xoá danh mục'
  ],
    'product' => [
        'index' => 'Hiển thị danh sách sản phẩm',
        'add' => 'Thêm mới sản phẩm',
        'edit' => 'Sửa sản phẩm',
        'delete' => 'Xoá sản phẩm'
    ],
    'bill' => [
        'index' => 'Hiển thị danh sách hoá đơn',
        'add' => 'Thêm mới hoá đơn',
        'edit' => 'Sửa hoá đơn',
        'delete' => 'Xoá hoá đơn'
    ],
    'detailbill' => [
        'index' => 'Hiển thị danh sách chi tiết hoá đơn',
        'add' => 'Thêm mới chi tiết hoá đơn',
        'edit' => 'Sửa chi tiết hoá đơn',
        'delete' => 'Xoá chi tiết hoá đơn'
    ],
    'post' => [
        'index' => 'Hiển thị danh sách bài viết',
        'add' => 'Thêm mới bài viết',
        'edit' => 'Sửa bài viết',
        'delete' => 'Xoá bài viết'
    ],
    'tag' => [
        'index' => 'Hiển thị danh sách tag',
        'add' => 'Thêm mới tag',
        'edit' => 'Sửa tag',
        'delete' => 'Xoá tag'
    ],
    'user' => [
        'index' => 'Hiển thị danh sách người dùng',
        'add' => 'Thêm mới người dùng',
        'edit' => 'Sửa người dùng',
        'delete' => 'Xoá người dùng'
    ],
    'code' => [
        'index' => 'Hiển thị danh sách mã giảm giá',
        'add' => 'Thêm mới mã giảm giá',
        'edit' => 'Sửa mã giảm giá',
        'delete' => 'Xoá mã giảm giá'
    ],
    'role' => [
        'index' => 'Hiển thị danh sách quyền',
        'add' => 'Thêm mới quyền',
        'edit' => 'Sửa quyền',
        'delete' => 'Xoá quyền'
    ],
    'ship' => [
        'index' => 'Hiển thị danh sách tỉnh thành',
        'add' => 'Thêm mới tỉnh thành',
        'edit' => 'Sửa tỉnh thành',
        'delete' => 'Xoá tỉnh thành'
    ],
    'banner' => [
        'index' => 'Hiển thị danh sách banner',
        'add' => 'Thêm mới banner',
        'edit' => 'Sửa banner',
        'delete' => 'Xoá banner'
    ],
    'comment' => [
        'index' => 'Hiển thị danh sách khách hàng',
        'add' => 'Thêm mới khách hàng',
        'edit' => 'Sửa khách hàng',
        'delete' => 'Xoá khách hàng'
    ],
    'contact' => [
        'index' => 'Hiển thị danh sách liên hệ khách hàng',
        'add' => 'Thêm mới liên hệ khách hàng',
        'edit' => 'Sửa liên hệ khách hàng',
        'delete' => 'Xoá liên hệ khách hàng'
    ],
    'info' => [
        'index' => 'Hiển thị thông tin chung',
        'edit' => 'Sửa thông tin chung',
    ],
    'menu' => [
        'index' => 'Hiển thị danh sách menu',
        'add' => 'Thêm mới menu',
        'edit' => 'Sửa menu',
        'delete' => 'Xoá menu'
    ],
    'policy' => [
        'index' => 'Hiển thị danh sách chính sách',
        'add' => 'Thêm mới chính sách',
        'edit' => 'Sửa chính sách',
        'delete' => 'Xoá chính sách'
    ],
    'partner' => [
        'index' => 'Hiển thị danh sách đối tác',
        'add' => 'Thêm mới đối tác',
        'edit' => 'Sửa đối tác',
        'delete' => 'Xoá đối tác'
    ],
    'checkout' => [
        'index' => 'Hiển thị danh sách thanh toán',
        'add' => 'Thêm mới thanh toán',
        'edit' => 'Sửa thanh toán',
        'delete' => 'Xoá thanh toán'
    ],

];


