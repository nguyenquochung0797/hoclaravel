<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '641919863742902',
        'client_secret' => 'd6ec13e69c6dd6b7264079243003203d',
        'redirect' => "http://192.168.1.81/signIn/facebook/callback",
    ],
    'google' => [
        'client_id'     => '689194084018-3117lvnnmlmau3qhj3k527uq1b7gdfvf.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect'      => 'http://localhost/signIn/google/callback',
    ],

];
