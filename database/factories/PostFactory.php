<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'name' => $faker->username,
        'content' => Hash::make(12345678),
        'category_id' => $faker->ean8,
    ];
});
