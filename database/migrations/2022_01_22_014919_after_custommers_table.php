<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AfterCustommersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custommers',function (Blueprint $table) {
            $table->date('birday')->nullable();
            $table->integer('sex')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custommers',function (Blueprint $table) {
            $table->dropColumn('birday');
            $table->dropColumn('sex');
        });
    }
}
