function submitForm(t) {
    var link = $(t).data("link");
    $("#form_data").attr("action", link);
    $("#form_data").submit();
}

function deleleMulti(t) {
    if (confirm("Bạn có muốn xoá ?")) {
        var link = $(t).data("link");
        $("#form_data").attr("action", link);
        $("#form_data").submit();
    }

}

function Del() {
    return confirm("Bạn có muốn xoá ?");
}

$("#name").change(function () {
    var str = $(this).val();
    str = str.toLowerCase();
    // xóa dấu
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
    str = str.replace(/(đ)/g, 'd');

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, '');

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, '-');

    // xóa phần dự - ở đầu
    str = str.replace(/^-+/g, '');
    // xóa phần dư - ở cuối
    str = str.replace(/-+$/g, '');
    // xóa -- lặp lại
    str = str.replace(/-+/g, '-');

    $("#slug").val(str);
});

$("#filter_status").change(function () {
    var v = $(this).val();
    window.location = "?fillter_status=" + v;
})

$("#checkall").click(function () {
    $(".checkClass").prop('checked', $(this).prop('checked'));
})

$("#fillter_order").change(function () {
    var v = $(this).val();
    window.location = "?fillter_order=" + v;
})

$("#fillter_category").change(function () {
    var v = $(this).val();
    window.location = "?fillter_category=" + v;
})

function sorting(field, sort) {
    sort = sort == "desc" ? "asc" : "desc";
    window.location = "?field=" + field + "&type=" + sort;
}

$('body').on('click', '#edit', function () {
    var id = $(this).data('id');
    var menu = $(this).data('menu');
    $.ajax({
        url: '/admin/'+menu+'/changeStatus/',
        data: {"id": id},
    }).done(function (data) {
        $('#changeStatus_'+id).html(data)
    })
})

$('#format').on('click',function () {
    var data = $(this).data('format');
    $('#form_format').attr('action',data).submit();
})

$('.province_name').on('click',function (){
    var id = $(this).data('id');
    $.ajax({
        url:'/admin/ship/district',
        data:{"id":id}
    }).done(function (data){
        $('#district_list').html(data)
    })
})

$('body').on('change','#price_Ship',function (){
    var id = $(this).data('id');
    //alert(id)
    var price = $(this).val();
    $.ajax({
        url:'/admin/ship/update',
        // type:'POST',
        data:{"id":id,"price":price}
    })
})
$('body').on('click','#btn-province',function (){
    var id = $('#id_province').val();
    var price = $('#price_province').val();
    $.ajax({
        url:'/admin/ship/updateProvince',
        // type:'POST',
        data:{"id":id,"price":price}
    }).done(function (data){
        $('#district_list').html(data)
    })
})
$('body').on('click','#btn-all',function (){
    var price = $('#price_all').val();
    $.ajax({
        url:'/admin/ship/updateAll',
        // type:'POST',
        data:{"price":price}
    })
})
$('body').on('change','.order',function (){
    var value = $(this).val();
    var id = $(this).data('id');
    var menu = $(this).data('menu');
    $.ajax({
        url: '/admin/'+menu+'/updateOrder',
        data: {'id':id,'value':value}
    })
})
$('body').on('change','.price',function (){
    var value = $(this).val();
    var id = $(this).data('id');
    $.ajax({
        url: '/admin/product/updatePrice',
        data: {'id':id,'value':value}
    })
})

$('body').on('change','.discount',function (){
    var value = $(this).val();
    var id = $(this).data('id');
    $.ajax({
        url: '/admin/product/updateDiscount',
        data: {'id':id,'value':value}
    })
})



