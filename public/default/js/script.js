function submitForm(t) {
    var link = $(t).data("link");
    $("#form_data").attr("action", link);
    $("#form_data").submit();
}

$('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 200, zoomHeight: 200, title: true, tint: '#333', Xoffset: 15});

$('.search').on('keyup', function () {
    $('.search-suggest div').hide();
    var search = $(this).val();
        $.ajax({
            url: '/searchFullText',
            data: {"search":search},
        }).done(function (data) {
                $('.search-suggest').html(data)
            })
})

$('body').on('click', '.quick_view', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/ajax/get-quick-view',
        type: 'GET',
        data: {"id": id},
        // success:function (data){
        //     $("body").append(data);
        //     $('#modalProductQuickView'+id).modal('show');
        // }
    }).done(function (data) {
        $("body").append(data);
        $('#modalProductQuickView' + id).modal('show');
    })
});

$('ul.pagination').hide();
$('.scrolling-pagination').jscroll({
    autoTrigger: true,
    //loadingHtml: '<img class="center-block" src="/default/images/xloading.gif" alt="Loading..." />',
    padding: 0,
    nextSelector: '.pagination li.active + li a',
    contentSelector: 'div.scrolling-pagination',
    callback: function () {
        // xóa thanh phân trang ra khỏi html mỗi khi load xong nội dung
        $('ul.pagination').remove();
    }
});


var btnUpload = $(".btn_upload");
btnUpload.on('click', function () {
    selectFileWithCKFinder(this);
})
function selectFileWithCKFinder(t) {
    CKFinder.popup({
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                var urlFile = file.getUrl();

                $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                $("input[name='picture_url']").val(urlFile);
            });

            finder.on('file:choose:resizedImage', function (evt) {

            });
        }
    });
}

$('#displayPassword').on('click', function () {
    $('.Pass').toggleClass('hidden');
})


// $('#code').on('change',function (){
//     var code = $(this).val();
//     var total = $('#total').val();
//     var money = total-(total*code)
//     var ship = $('#ship').val();
//     var totalPrice = Number(money)+Number(ship);
//     $('.discount').html(new Intl.NumberFormat('en-US', {style: 'percent'}).format(code));
//     $('.money').html(new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(money)+'đ');
//     $('.total-price').html(new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(totalPrice)+'đ');
//     $('#price').attr('value',new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(totalPrice));
// })
// $('#ship').on('change',function (){
//     var code = $('#code').val();
//     var total = $('#total').val();
//     var money = total-(total*code)
//     var ship = $(this).val();
//     var totalPrice = Number(money)+Number(ship);
//     $('.ship').html(new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(ship)+'đ')  ;
//     $('.money').html(new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(money)+'đ');
//     $('.total-price').html(new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(totalPrice)+'đ');
//     $('#price').attr('value',new Intl.NumberFormat('en-US', { maximumSignificantDigits:4}).format(totalPrice));
// })

$('body').on('click', '#quichViewCart', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/quickviewCart',
        data: {"id": id}
    }).done(function (data) {
        $('body').append(data);
        $('#viewCart').modal('show');
    })

})
$('#btn-code').on('click', function () {
    var code = $('#code').val();
    $.ajax({
        url: '/code',
        data: {'code': code}
    }).done(function (data) {
        $('.val_code').val(data.trim());
        var val_code = $('.val_code').val();
        var total = $('#total').val();
        var money = 0;
        var unit = '';
        var discount = Number(val_code);
        if (discount > 100) {
            unit = 'đ'
            money = total - discount
        } else {
            unit = '%'
            money = total - (total * discount / 100)
        }
        var ship = $(".val_ship").val();
        var totalPrice = Number(money) + Number(ship);
        $('.money').html(new Intl.NumberFormat('en-US', {maximumSignificantDigits: 4}).format(money) + 'đ');
        $('.total-price').html(new Intl.NumberFormat('en-US', {maximumSignificantDigits: 4}).format(totalPrice) + 'đ');
        $('#price').attr('value', totalPrice);
        if (data > 100) {
            data = new Intl.NumberFormat('en-US', {maximumSignificantDigits: 4}).format(data)
        }
        $('.discount').html(data + unit)
    })
})

$('#province').on('change', function () {
    var province = $(this).val();
    $.ajax({
        url: '/district',
        data: {"province": province}
    }).done(function (data) {
        //console.log(data)
        $('#district').html(data);
    })
})

$('#district').on('change', function () {
    var district = $(this).val();
    var price_ship = $("#district option[value=" + district + "]").data('price');
    $('.ship').html(new Intl.NumberFormat('en-US', {maximumSignificantDigits: 4}).format(price_ship) + 'đ');
    var total = $('#total').val();
    var code = $('.val_code').val();
    $('.val_ship').val(price_ship);
    if (code > 100) {
        unit = 'đ'
        money = total - code
    } else {
        unit = '%'
        money = total - (total * code / 100)
    }
    var totalPrice = Number(money) + Number(price_ship);
    $('.total-price').html(new Intl.NumberFormat('en-US', {maximumSignificantDigits: 4}).format(totalPrice) + 'đ');
    $('#price').attr('value', totalPrice);
    $.ajax({
        url: '/ward',
        data: {"district": district}
    }).done(function (data) {
        $('#ward').html(data);
    })
})
$('#ward').on('change', function () {
    var street = $('#street').val();
    var province_id = $('#province').val();
    var province = $("#province option[value=" + province_id + "]").text();
    var district_id = $('#district').val();
    var district = $("#district option[value=" + district_id + "]").text();
    var ward_id = $(this).val();
    var ward = $("#ward option[value=" + ward_id + "]").text();
    $('#address').val(street + ", Phường " + ward + ", " + district + ", " + province);
})

$('body').on('click', '#wishList', function () {
    var id = $(this).data('id');
    $(this).attr('style', 'backgourd:red')
    $.ajax({
        url: '/danh-sach-yeu-thich',
        data: {"id": id}
    }).done(function () {
        $('.wishList_' + id)
            .html("<a style='background: red' href='javascript:void()' class='heart d-flex justify-content-center align-items-center'><span><i class='ion-ios-heart abc'></i></span></a>")
    })
})
$('body').on('click', '#delete_wishList', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/xoa-danh-sach-yeu-thich',
        data: {"id": id}
    }).done(function (data) {
        $('#delete_wish_List').html(data)
    })
})
$('body').on('click', '#addCart', function () {
    var id = $(this).data('id');
    var quantily = $("input[name=quantity]").val();
    $.ajax({
        url: '/them-gio-hang',
        data: {"id": id, "quantily": Number(quantily)}
    }).done(function (data) {
        $('.addCartQuick_' + id)
            .html("<a style='background: cadetblue' href='javascript:void()' class='buy-now d-flex justify-content-center align-items-center mx-1'><span><i class='ion-ios-cart'></i></span></a>")
        $('#quantily_cart').html("<span style='color: red;font-size: 12px' class='icon-shopping_cart'></span>[" + data.trim() + "]")
        $('#val_count_cart').val(data.trim())
    })
})

$('body').on('click', '#delete_cart_all', function () {
    $.ajax({
        url: '/xoa-tat-ca-gio-hang',
    }).done(function (data) {
        $('#delete_cart').html('<tr><td><h3>Giỏ hàng rỗng</h3></td></tr>')
        $('#quantily_cart').html("<span style='color: red;font-size: 12px' class='icon-shopping_cart'></span>[0]")
    })
})

$('body').on('click','#delete_cart_item', function () {
    var id = $(this).data('id');
    var x = $('#val_count_cart').val();
    var quantily = Number(x)-1;
    $.ajax({
        url: '/xoa-gio-hang',
        data: {"id": id}
    }).done(function (data) {
        $('#delete_cart').html(data);
        $('#quantily_cart').html("<span style='color: red;font-size: 12px' class='icon-shopping_cart'></span>["+quantily+"]");
        $('#val_count_cart').val(quantily);
     })
})
$('body').on('click','#subnav',function (){
    var id = $(this).data('id');
    var link = $(this).data('link');
    $.ajax({
        url:'/productAjax',
        data:{"id":id}
    }).done(function (data){
        $('.product-ajax').html(data);
        window.history.pushState(null, null, link);
    })
})

