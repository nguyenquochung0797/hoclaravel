<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="">
    @yield('head')
    <link rel="icon" href="{{ asset('default/images/product-1.jpg')}}"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">

    <link rel="stylesheet" href="{{asset('default/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('default/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('default/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('default/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('default/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('default/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('default/css/formSignUp.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/profileUser.css')}}">
    <link rel="stylesheet" href="{{asset('default/css/css.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('default/css/xzoom.css')}}">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">


</head>
