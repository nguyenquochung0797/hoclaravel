@php
    $policies = \App\Policy::where('status',1)->get();
    $menu = \App\Menu::where('status',1)->get();
    $info = \App\Info::find(1);
@endphp
<div class="container">
    <div class="row">
        <div class="mouse">
            <a href="#" class="mouse-icon">
                <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
            </a>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Restaurant</h2>
                <p>Khách hàng không mong đợi bạn hoàn hảo, họ mong đợi bạn khắc phục sai lầm của họ để nó trở nên hoàn hảo.</p>
                <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                    <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
                <h2 class="ftco-heading-2">Menu</h2>
                <ul class="list-unstyled">
                    @foreach($menu as $m)
                    <li><a href="{{route('showPage',['slugCategory'=>$m->slug])}}" class="py-2 d-block">{{$m->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Chính sách khách hàng</h2>
                <div class="d-flex">
                    <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                        @foreach($policies as $policy)
                        <li><a href="#" class="py-2 d-block">{{$policy->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Liên Hệ</h2>
                <div class="block-23 mb-3">
                    <ul>
                        <li><span class="icon icon-map-marker"></span><span class="text">{{$info->address}}</span></li>
                        <li><a href="#"><span class="icon icon-phone"></span><span class="text">{{$info->phone}}</span></a></li>
                        <li><a href="#"><span class="icon icon-envelope"></span><span class="text">{{$info->email}}</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="#" target="_blank">QuocHung</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
    </div>
</div>
