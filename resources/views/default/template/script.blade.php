<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
{{--<script src="{{asset('default/js/jquery.min.js')}}"></script>--}}
<script src="{{asset('default/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('default/js/popper.min.js')}}"></script>
<script src="{{asset('default/js/bootstrap.min.js')}}"></script>
<script src="{{asset('default/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('default/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('default/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('default/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('default/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('default/js/aos.js')}}"></script>
<script src="{{asset('default/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('default/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('default/js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('default/js/google-map.js')}}"></script>
<script src="{{asset('default/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('default/js/xzoom.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}
<script src="{{asset('default/js/script.js')}}"></script>


