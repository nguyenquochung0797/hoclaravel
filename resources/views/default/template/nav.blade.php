@php
    $menu_food = \App\Category::where('status',1)->get();
    $memu_main = \App\Menu::where('status',1)->get();
    $info = \App\Info::find(1);
    $checkout = \App\Checkout::where('status',1)->orderBy('order','desc')->get();
@endphp
<div class="container">
    <a class="navbar-brand" href="{{ route('home') }}">Restaurant</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="{{ route('home') }}" class="nav-link">Trang Chủ</a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" >Thực Phẩm</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                    @foreach($menu_food as $menu)
                    <a class="dropdown-item" href="{{route('productDetail',['slugProduct'=>$menu->slug])}}">{{$menu->name}}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown">Thanh Toán</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
{{--                    @foreach($checkout as $list)--}}
{{--                         <a class="dropdown-item" href="{{ route('show',['slug'=>$list->slug]) }}">{{$list->name}}</a>--}}
{{--                    @endforeach--}}
                    <a class="dropdown-item" href="{{ route('showCart') }}">Giỏ Hàng</a>
                    <a class="dropdown-item" href="{{ route('showCheckout') }}">Thanh Toán</a>
                    <a class="dropdown-item" href="{{ route('showWishList') }}">Danh Sách Yêu Thích</a>
                </div>
            </li>
            @foreach($memu_main as $menumain)
            <li class="nav-item"><a href="{{ route('showPage',['slugCategory'=>$menumain->slug]) }}" class="nav-link">{{$menumain->name}}</a></li>
            @endforeach
            <li class="nav-item cta cta-colored"><a style="color: black;font-size: 12px" href="{{ route('showCart') }}" class="nav-link" id="quantily_cart"><span style="color: red;font-size: 12px" class="icon-shopping_cart"></span id="quantily_cart">[{{\Cart::session('bill')->getContent()->count()}}]</a></li>
            <input type="hidden" value="{{\Cart::session('bill')->getContent()->count()}}" id="val_count_cart">
        </ul>
    </div>
</div>
