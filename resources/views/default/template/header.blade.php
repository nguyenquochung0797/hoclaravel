@php
    $info = \App\Info::find(1);
    $custommer = \App\Custommer::find(session()->get('IDUser'))
@endphp
<div class="container">
    <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
        <div class="col-lg-12 d-block">
            <div class="row d-flex">
                <div class="col-md pr-4 d-flex topper align-items-center">
                    <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                    <span class="text">{{$info->email}}</span>
                </div>
                <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                    <span class="text">Giao hàng 3-5 ngày làm việc & Trả hàng miễn phí</span>
                </div>
                <div class="col-md pr-4 d-flex topper align-items-center">
                    <div class="icon mr-2 d-flex justify-content-center align-items-center"></div>
                    @if(session()->has('loginSuccess'))
                        <div style="margin-right: 10px"><img style="width: 20px;height: 20px;border-radius: 10px" src="{{$custommer->picture??'/userfiles/images/custommer/user-custommer.png'}}" alt=""></div>
                        <div style="margin-right: 15px"><a href="{{route('profileUser')}}"><span style="color: black " class="text">{{ $custommer->name }}</span></a></div>
                        <div><a href="{{route('signOut')}}"><span class="text" >Đăng Xuất</span></a></div>
                    @else
                    <div id="divsignUp" style="margin-right: 15px"><a href="{{route('signUp')}}"><span class="text" id="signUp">Đăng Kí</span></a></div>
                    <div id="divsignIn"><a href="{{route('signIn')}}"><span class="text">Đăng Nhập</span></a></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
