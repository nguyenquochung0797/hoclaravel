<!DOCTYPE html>
<html lang="en">
<head>
    @include("default.template.head")
</head>
<body class="goto-here">
    <div class="py-1 bg-primary">
        @include("default.template.header")
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include("default.template.nav")
    </nav>
    @include('sweetalert::alert')

    @yield('content')
    <footer class="ftco-footer ftco-section">
        @include("default.template.footer")
    </footer>
  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
        @include("default.template.script")
  </body>
</html>
