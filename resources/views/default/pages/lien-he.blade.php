@extends('default.layout')
@section('head')
    <title>Liên Hệ</title>
@stop
@php
    $info = \App\Info::find(1);
@endphp

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url({{$info->pictureContact}});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a style="font-weight: bold ;color: black" href="index.html">Trang</a></span>
                        <span style="font-weight: bold ;color: black">Liên Hệ</span></p>
                    <h1 class="mb-0 bread" style="color: #0000cc">Liên Hệ Với Chúng Tôi</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <div class="row d-flex mb-5 contact-info">
                <div class="w-100"></div>
                <div class="col-md-3 d-flex">
                    <div class="info bg-white p-4">
                        <p class="p-4-1"><span>Địa chỉ</span></p>
                        <span>{{$info->address}}</span>
                    </div>
                </div>
                <div class="col-md-2 d-flex">
                    <div class="info bg-white p-4">
                        <p class="p-4-1"><span>Số ĐT</span></p>
                        <span>{{$info->phone}}</span>
                    </div>
                </div>
                <div class="col-md-4 d-flex">
                    <div class="info bg-white p-4">
                        <p class="p-4-1"><span>Email</span> </p>
                        <p>{{$info->email}}</p>
                    </div>
                </div>
                <div class="col-md-3 d-flex">
                    <div class="info bg-white p-4">
                        <p class="p-4-1"><span>Website</span> </p>
                        <span>{{$info->web}}</span>
                    </div>
                </div>
            </div>
            <div class="row block-9">
                <div class="col-md-6 order-md-last d-flex">
                    <form action="{{route('storeContact')}}" method="POST" class="bg-white p-5 contact-form">
                        @csrf
                        <div class="form-group">
                            <input name="name" type="text" class="form-control" placeholder="Họ và Tên">
                        </div>
                        <div class="form-group">
                            <input name="email" type="text" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input name="phone" type="text" class="form-control" placeholder="Số Điện Thoại">
                        </div>
                        <div class="form-group">
                            <textarea name="contentContact" id="" cols="30" rows="7" class="form-control"
                                      placeholder="Nội Dung"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Gửi" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>

                <div class="col-md-6 d-flex">
                    <div id="map" class="bg-white"></div>
                </div>
            </div>
        </div>
    </section>


@stop
