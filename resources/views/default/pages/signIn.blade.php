@extends('default.layout')
@section('content')
    <section style="width: 500px" class="get-in-touch">
        <h1 class="title">Đăng Nhập</h1>
        <form class="contact-form row" id="form-SignUp" method="POST" action="{{route('checksignIn')}}">
            @csrf
            <div class="form-field col-lg-12">
                <input id="name" class="input-text js-input" type="text" name="username">
                <label class="label" for="name">Username hoặc Email</label>
                @if($errors->first('username'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('username') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12">
                <input id="email" class="input-text js-input" name="password" type="password">
                <label class="label" for="email">Mật khẩu</label>
                @if($errors->first('password'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('password') }}</p></small>
                @endif
            </div>
            @if(session()->has('checkLogin'))
                <div class="alert alert-danger">
                    {{ session()->get('checkLogin') }}
                </div>
            @endif
            <div class="form-field col-lg-12" style="text-align: center">
                <button id="btnSubmitSignIn" class="submit-btn">Đăng Nhập</button>
            </div>
        </form>
        <div class="text" style="text-align: center">
        <p style="margin-bottom: 10px; margin-top: -15px;"><a href="{{route('forgetPass')}}">Quên mật khẩu ?</a></p>
        <p style="margin-bottom: 5px;">Chưa có tài khoản? <a href="{{route('signUp')}}">Tạo tài khoản ngay</a></p>
        <p style="margin-bottom: 20px;">Hoặc <span>Đăng nhập bằng</span></p>
        </div>
        <div class="form-group half" style="padding-bottom: 10px">
            <div class="row">
                <div class="form-item col-lg-6">
                    <a style="height: 40px;color: #fff;background: #3b5998;border-radius: 30px;"
                       href=" {{route('signInFB')}} "
                       class="d-flex align-items-center justify-content-center facebook">
                        <img style="width: 20px"src="https://anyclass.vn/enduser/assets/icons/icon-facebook-white.svg" alt="">Facebook</a>
                </div>
                <div class="form-item  col-lg-6">
                    <a style="height: 40px;color: #fff;background: #d7342d;border-radius: 30px;"
                       class="d-flex align-items-center justify-content-center google"
                       href="/signIn/google">
                        <img style="width: 20px"src="https://anyclass.vn/enduser/assets/icons/icon-google-white.svg"alt="">Google</a>
                </div>
            </div>
        </div>
        <div class="form-group" style="margin-top: -15px;">
            <div class="form-item">
                <a style="height: 40px;color: #fff;background: #2792f0;border-radius: 30px;"
                   href="" class="d-flex align-items-center justify-content-center zalo"><img
                        src="https://anyclass.vn/enduser/assets/icons/icon-zalo-white.svg" alt="">Zalo</a>
            </div>
        </div>
    </section>
@stop

