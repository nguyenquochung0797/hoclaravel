@extends('default.layout')
@section('head')
    <title>Tin Tức</title>
@stop
@php
    $info = \App\Info::find(1);
    $posts = \App\Post::where('status',1)->get();
@endphp
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url({{$info->pictureNews}});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center banner-product">
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <div class="row">
                        @foreach($posts_news as $post)
                            <div class="col-md-12 d-flex ftco-animate news">
                                <div class="blog-entry align-self-stretch d-md-flex">
                                    <a href="{{route('showNews',['slugNew'=>$post->slug])}}" class="block-20"
                                       style="background-image: url({{$post->picture}});">
                                    </a>
                                    <div class="text d-block pl-md-4">
                                        <div class="meta mb-3">
                                            <div>{{$post->updated_at->format('d/m/Y-h:i A')}}</div>
                                            <div>{{$post->user->name}}</div>
                                        </div>
                                        <h3 class="heading"><a
                                                href="{{route('showNews',['slugNew'=>$post->slug])}}">{{$post->name}}</a>
                                        </h3>
                                        <p>{{$post->meta_description}}</p>
                                        <p><a href="{{route('showNews',['slugNew'=>$post->slug])}}"
                                              class="btn btn-primary py-2 px-3">Xem thêm</a></p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
{{--                        @if($total_page>1)--}}
{{--                            <div class="row mt-5">--}}
{{--                                <div class="col text-center">--}}
{{--                                    <div class="block-27">--}}
{{--                                        <ul>--}}
{{--                                            @if($page_current>1)--}}
{{--                                                <li><a href="{{route('search')}}?page={{$page_current-1}}">&lt;</a>--}}
{{--                                                </li>@endif--}}
{{--                                            @for($i=1;$i<=$total_page;$i++)--}}
{{--                                                <li @if ($i==$page_current) class="active" @endif><span><a--}}
{{--                                                            href="{{route('search')}}?page={{$i}}">{{$i}}</a></span></li>--}}
{{--                                            @endfor--}}
{{--                                            @if($page_current<$total_page)--}}
{{--                                                <li><a href="{{route('search')}}?page={{$page_current+1}}">></a>--}}
{{--                                                </li>@endif--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                    </div>
                </div>
                <div class="col-lg-4 sidebar ftco-animate">
                    <div class="sidebar-box">
                        <div class="form-group">
                            <div class="header-search">
                                <form method="POST" id="header-search" >
                                    @csrf
                                    <input type="text" name="search" class="form-control m-input" placeholder="Tìm kiếm" autocomplete="off" />
                                </form>
                            </div>
                            <div id="search-suggest" class="s-suggest"></div>
                        </div>
                    </div>
                    <div class="sidebar-box ftco-animate">
                        <h3 class="heading">Recent Blog</h3>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url(images/image_1.jpg);"></a>
                            <div class="text">
                                <h3 class="heading-1"><a href="#">Even the all-powerful Pointing has no control about
                                        the blind texts</a></h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span> April 09, 2019</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

