@extends('default.layout')
@section('head')
    <title>Giỏ Hàng</title>
@stop

@section('content')
    @include('default.component.banner-checkout',['banner'=>\App\Checkout::where('status',1)->where('slug','gio-hang')->first()]);
    <section class="ftco-section ftco-cart">
        <div class="container">
            <h1 style="text-align: center;color: #761b18">Giỏ Hàng</h1>
            <form action="{{ route('showCheckout') }}" class="info">
                <div class="row">
                    <div class="col-md-12 ftco-animate">
                        <div class="cart-list">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>&nbsp;</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Tổng tiềnl</th>
                                </tr>
                                </thead>
                                <tbody id="delete_cart">
                                <div class="alert">

                                </div>
                                @if (isset($items))
                                    @php  $total = 0; @endphp
                                    @foreach($items as $item)
                                        <tr class="text-center">
                                            <td class="product-remove">
                                                <a data-id="{{$item->id}}" href="javascript:void()"
                                                   id="delete_cart_item">
                                                    <span class="ion-ios-close"></span>
                                                </a>
                                                <input type="hidden" name="detail[{{$item->id}}][id_product]"
                                                       value="{{$item->id}}">
                                            </td>
                                            <td class="image-prod">
                                                <div class="img"
                                                     style="background-image:url({{$item['conditions']['picture']}});"></div>
                                                <input type="hidden" name="detail[{{$item->id}}][picture_product]"
                                                       value="{{$item['conditions']['picture']}}">
                                            </td>

                                            <td class="product-name">
                                                <h3>{{$item->name}}</h3>
                                                {{--                                                 <p>{!! $item['conditions']['content'] !!}</p>--}}
                                                <input type="hidden" name="detail[{{$item->id}}][name_product]"
                                                       value="{{$item->name}}">
                                            </td>

                                            <td class="price">{{number_format($item->price)}}đ</td>
                                            <input type="hidden" name="detail[{{$item->id}}][price_product]"
                                                   value="{{$item->price}}">

                                            <td class="quantity">
                                                <div class="input-group mb-3">
                                                    <input type="text" name="quantity"
                                                           class="quantity form-control input-number"
                                                           value="{{$item->quantity}}" readonly>
                                                    <input type="hidden" name="detail[{{$item->id}}][quantity_product]"
                                                           value="{{$item->quantity}}">
                                                </div>
                                            </td>

                                            <td class="total">{{number_format(\Cart::get($item->id)->getPriceSum())}}đ
                                            </td>
                                            <input type="hidden" name="detail[{{$item->id}}][total_product]"
                                                   value="{{\Cart::get($item->id)->getPriceSum()}}">
                                        </tr>
                                        @php $total += \Cart::get($item->id)->getPriceSum(); @endphp
                                        <input type="hidden" name="total"
                                               value="{{$total}}">
                                    @endforeach
                                @endif
                                @if (isset($items) && count($items)>=1)
                                    <tr>
                                        <td>
                                            <a href="javascript:void()" id="delete_cart_all"><input type="button"
                                                                                                    class="btn btn-danger"
                                                                                                    value="Xoá giỏ hàng"></a>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>
                                            <h3>Giỏ hàng rỗng</h3>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="text-align: center" class="error">
                    @if($errors->first('total'))
                        <h1 class="text-danger">{{ $errors->first('total') }}</h1>
                    @endif
                </div>
                <div class="row justify-content-end">
                    <div class="col-lg-3 mt-5 cart-wrap ftco-animate">
                        <div class="cart-total mb-3">
                            <h3>Tổng thanh toán</h3>
                            <p class="d-flex">
                                <span>Tạm tính</span>
                                <span style="text-align: right;">{{number_format($total??0)}}đ</span>
                            </p>
                            <p class="d-flex">
                                <span>Phí giao hàng</span>
                                <span style="text-align: right;">0đ</span>
                            </p>
                            <p class="d-flex">
                                <span>Discount</span>
                                <span style="text-align: right;">0%</span>
                            </p>
                            <hr>

                            <p class="d-flex total-price">
                                <span>Tổng</span>
                                <span style="text-align: right;">{{number_format($total??0)}}đ</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <input type="submit" value="Thanh toán" class="btn btn-primary py-3 px-4">
                </div>
            </form>
        </div>
    </section>

@stop
