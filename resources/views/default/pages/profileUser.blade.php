@extends('default.layout')
@section('content')
    @php
        $custommer = \App\Custommer::find(session()->get('IDUser'));
        $bills = \App\Bill::where('custommer_id',$custommer->id)->get();
    @endphp
    <div class="container">
        <div class="row gutters">
            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="account-settings">
                            <div class="user-profile">
                                <div class="form-group">
                                    <div class="preview_image">
                                        <img
                                            src="{{(old('picture_url')==null)?$custommer->picture:(old('picture_url'))}}"
                                            style="width: 200px; height:200px;border-radius: 50%;margin-bottom: 10px">
                                    </div>
                                    <div class="action_image">
                                        <button type="button" class="btn btn-info btn_upload">Chọn ảnh</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="py-3">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="ml-2">&nbsp;</li>
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><h6
                                            class="mb-2 text-primary">Thông tin cá nhân</h6></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Đơn hàng</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane p-2 active" id="home" role="tabpanel">
                                    <form action="{{route('storeprofileUser',['id'=>$custommer->id])}}" method="POST">
                                        @csrf
                                        <div class="row gutters">
                                            <input id="picture" type="hidden" name="picture_url"
                                                   value="{{$custommer->picture??old('picture_url')}}">
                                            <input type="hidden" id="id" name="id" value="{{$custommer->id}}">
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="fullName">Họ và tên</label>
                                                    <input type="text" class="form-control" id="fullName"
                                                           placeholder="Họ và tên"
                                                           name="name" value="{{$custommer->name}}">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="eMail">Email</label>
                                                    <input type="email" class="form-control" id="eMail"
                                                           placeholder="email@gmail.com"
                                                           name="email" value="{{$custommer->email}}">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="phone">Số điện thoại</label>
                                                    <input type="tel" class="form-control" id="phone"
                                                           placeholder="Số điện thoại"
                                                           name="phone" value="{{$custommer->phone}}">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="Address">Địa chỉ</label>
                                                    <input type="text" class="form-control" id="address"
                                                           placeholder="Địa chỉ"
                                                           name="address" value="{{$custommer->address}}">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label>Ngày sinh</label>
                                                    <input type="date" class="form-control" value="{{$custommer->birday}}" name="date">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label>Giới tính</label>
                                                    <div class="form-group">
                                                        <label><input @if ($custommer->sex==0) checked @endif type="radio" name="gt" value="0">Nam</label>
                                                        <label><input @if ($custommer->sex==1) checked @endif type="radio" name="gt" value="1">Nữ</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row gutters">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="text-right">
                                                    <button type="submit" id="submitprofileUser"
                                                            class="btn btn-primary">
                                                        Lưu
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row gutters">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <a href="javascript:void()" id="displayPassword"><h6
                                                    class="mt-3 mb-2 text-primary">Đổi
                                                    mật khẩu</h6></a>
                                            @if(session()->has('changePassSuccess'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('changePassSuccess') }}
                                                </div>
                                            @endif
                                            @if(session()->has('changePassFail'))
                                                <div class="alert alert-danger">
                                                    {{ session()->get('changePassFail') }}
                                                </div>
                                            @endif
                                            <div class="Pass hidden">
                                                <form action="{{route('changePassUser',['id'=>$custommer->id])}}"
                                                      method="POST">
                                                    @csrf
                                                    <div class="row gutters">
                                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <div class="form-group">
                                                                <label for="Street">Mật khẩu</label>
                                                                <input type="password" class="form-control" id="Street"
                                                                       placeholder="Nhập mật khẩu" name="password_old">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <div class="form-group">
                                                                <label for="ciTy">Mật khẩu mới</label>
                                                                <input type="password" class="form-control" id="ciTy"
                                                                       placeholder="Nhập mật khẩu mới"
                                                                       name="password_new">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary">Đổi mật
                                                                    khẩu
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane p-2" id="profile" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-wrap">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Tên</th>
                                                        <th>Thời gian đặt hàng</th>
                                                        <th>Tổng tiền</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $i =1;
                                                    @endphp
                                                    @foreach($bills as $bill)
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{{$bill->name}}</td>
                                                            <td>{{$bill->created_at->format('d/m/Y - h:i A')}}</td>
                                                            <td>{{number_format($bill->totalBill)}}đ</td>
                                                            @php
                                                                $icon_status = [
                                                                        '0' => 'fa-exclamation',
                                                                        '1' => 'fa-truck',
                                                                        '2' => 'fa-check-circle',
                                                                        '3' => 'fa-times',
                                                                ];
                                                                $color_status = [
                                                                        '0' => 'black',
                                                                        '1' => 'blue',
                                                                        '2' => 'green',
                                                                        '3' => 'red',
                                                                ];
                                                                $name_status = [
                                                                        '0' => 'Chờ xác nhận',
                                                                        '1' => 'Đang vận chuyển',
                                                                        '2' => 'Đã hoàn thành',
                                                                        '3' => 'Đã huỷ đơn',
                                                                ]
                                                            @endphp
                                                            <td> <a href=""><i
                                                                        style="font-size: 20px; color: @foreach($color_status as $key => $value) @if ($key==$bill->status){{$value}}@endif @endforeach"
                                                                        class="edit fa fa-fw @foreach($icon_status as $key => $value) @if ($key==$bill->status){{$value}}@endif @endforeach "></i>
                                                                    @foreach($name_status as $key => $value) @if ($key==$bill->status){{$value}}@endif @endforeach
                                                                </a>
                                                            </td>
                                                            @if($bill->status == 0)
                                                            <td><a href="{{route('admin.bill.changeBill',['id'=>$bill->id])}}" class="btn btn-danger">Huỷ Đơn</a></td>
                                                            @endif
                                                            <td><a data-id="{{$bill->id}}" href="javascript:void()" id="quichViewCart" data-toggle="modal" data-target="#viewCart"><i class="fa fa-eye"></i></a></td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane p-2" id="messages" role="tabpanel">Messages tab.</div>
                                <div class="tab-pane p-2" id="settings" role="tabpanel">Settings tab.</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('ckfinder::setup')
@stop


