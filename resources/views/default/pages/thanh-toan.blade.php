@extends('default.layout')

@section('head')
    <title>Thanh Toán</title>
@stop
@section('content')
    @php
        $custommer = \App\Custommer::find(session()->get('IDUser'));
        $codes = \App\Code::where('status',1)->get();
        $total = $bill['total'];
    @endphp
    @include('default.component.banner-checkout',['banner'=>\App\Checkout::where('status',1)->where('slug','thanh-toan')->first()]);
    <hr class="mb-4">
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Giỏ hàng</span>
                    <span class="badge badge-secondary badge-pill">{{count($items)}}</span>
                </h4>
                <ul class="list-group mb-3 sticky-top">
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Thành tiền</h6>
                        </div>
                        <span class="text-muted">{{number_format($bill['total'])}}đ</span>
                        <input type="hidden" value="{{$bill['total']}}" id="total">
                    </li>
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="text-success">
                            <h6 class="my-0">Mã giảm giá</h6>
                        </div>
                        <span class="text-success discount">0</span>
                        <input type="hidden" value="0" class="val_code">
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Tạm tính</h6>
                            <small class="text-muted">Brief description</small>
                        </div>
                        <span class="text-muted money">0đ</span>
                        <input type="hidden" value="0" class="val_money">
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">Vận chuyển</h6>
                        </div>
                        <span class="text-muted ship">0đ</span>
                        <input type="hidden" value="0" class="val_ship">
                    </li>
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="text-info">
                            <h6 class="my-0">Tổng tiền</h6>
                        </div>
                        <span class="text-info total-price">{{number_format($total)}}đ</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Thanh toán</h4>
                <form class="needs-validation" method="POST" action="{{route('admin.bill.store')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="username">Họ và Tên</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="bi bi-person"></i></span>
                            </div>
                            <input name="name" value="{{$custommer->name??old('name')}}" type="text" class="form-control" id="username" placeholder="Họ và tên" >
                        </div>
                        @if($errors->first('name'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('name') }}</p></small>
                        @endif
                    </div>
                    <input type="hidden" id="price" name="totalBill" value="{{$total}}">
                    <input type="hidden"  name="id" value="{{$custommer->id}}">
                    <div class="mb-3">
                        <label for="username">Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="bi bi-envelope"></i></span>
                            </div>
                            <input  name="email" value="{{$custommer->email??old('email')}}"type="text" class="form-control" id="username" placeholder="Email" required="">
                        </div>
                        @if($errors->first('email'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('email') }}</p></small>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="username">Điện thoại</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="bi bi-telephone"></i></span>
                            </div>
                            <input name="phone" value="{{$custommer->phone??old('phone')}}"type="text" class="form-control" id="username" placeholder="Điện thoại" required="">
                        </div>
                        @if($errors->first('phone'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('phone') }}</p></small>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="username">Số nhà, Đường</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="bi bi-house-door"></i></span>
                            </div>
                            <input id="street" value="" type="text" class="form-control" placeholder="Số nhà, Đường" required="">
                        </div>
                        @if($errors->first('address'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('address') }}</p></small>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="username">Thành phố/Tỉnh</label>
                        <div class="input-group">
                            @php
                                $provinces =\App\Province::all();
                            @endphp
                            <select name="province" class="custom-select d-block w-100" id="province" required="">
                                <option value="0">---Chọn Thành Phố/Tỉnh---</option>
                                @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('province'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('province') }}</p></small>
                        @endif
                    </div>

                    <div class="mb-3">
                        <label for="username">Quận/Huyện</label>
                        <div class="input-group">
                            <select name="district"  class="custom-select d-block w-100" id="district" required="">
                                <option value="0">---Chọn Quận/Huyện---</option>
                            </select>
                        </div>
                        @if($errors->first('district'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('district') }}</p></small>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="username">Phường/Xã</label>
                        <div class="input-group">
                            <select name="ward" class="custom-select d-block w-100" id="ward" required="">
                                <option value="0">---Chọn Phường/Xã---</option>
                            </select>
                        </div>
                        @if($errors->first('ward'))
                            <small id="emailHelp" class="form-text text-muted"><p
                                    class="text-danger">{{ $errors->first('ward') }}</p></small>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="username">Địa chỉ đầy đủ</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="bi bi-house-door"></i></span>
                            </div>
                            <input name="address" value="{{$custommer->address??old('address')}}" type="text" class="form-control" id="address" placeholder="Địa chỉ" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="state">Mã giảm giá</label>
                            <div class="input-group mb-3">
                                <input id="code" name="code" type="text" class="form-2 form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" value="{{session()->get('code')??""}}">
                                <div class="input-group-prepend">
                                    <button id="btn-code" class="btn btn-outline-secondary" type="button">Áp dụng</button>
                                </div>
                            </div>
                            <div class="invalid-feedback"> Vui lòng chọn mã giảm giá. </div>
                        </div>

                    </div>
                    <hr class="mb-4">
                    <button class="btn btn-info btn-lg btn-block" type="submit">Thanh toán</button>
                </form>
            </div>
        </div>
    </div>
@stop
