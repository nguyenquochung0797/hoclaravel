@php
    $infoBill = \App\Bill::find($id);
@endphp
<h3>{{$infoBill->name}} thân mến, cảm ơn bạn đã ghé thăm cửa hàng và mua hàng tại cửa hàng của chúng tôi!
Chúng tôi rất vui vì bạn đã tìm thấy những gì bạn đang tìm kiếm</h3>
<p>Bạn đã đặt thành công đơn hàng, vui lòng kiểm tra thông tin và nhấn vào xác nhận đơn hàng phía dưới. Sau 60p đơn hàng
sẽ bị xoá nếu bạn không xác nhận</p>
<div class="row">
    <div style="text-align: center">
        <h2>Thông tin đơn hàng </h2>
    </div>
    <div>
        <h3>Họ và tên: <span>{{$infoBill->name}}</span></h3>
        <h3>Số điện thoại: <span>{{$infoBill->phone}}</span></h3>
        <h3>Email: <span>{{$infoBill->email}}</span></h3>
        <h3>Địa chỉ: <span>{{$infoBill->address}}</span></h3>
        <h3>Tổng tiền: <span>{{number_format($infoBill->totalBill)}}đ</span></h3>
    </div>
</div>

<a href="http://192.168.1.81/confirmCart/{{$id}}/{{$token}}?email={{$email}}">Xác nhận tại đây</a>
<h4>Cảm ơn bạn một lần nữa và chúc bạn có một ngày tuyệt vời!</h4>
