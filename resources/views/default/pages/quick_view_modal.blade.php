<div class="modal fade" id="modalProductQuickView{{$product->id}}"  tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{$product->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-lg-6">
                    <img style="height: 300px; width: 350px; border-radius: 30px" src="{{$product->picture}}" alt="">
                </div>
                <div class="col-lg-6">
                    <form action="{{ route('addCart',['detail'=>$product->slug]) }}" method="POST">
                        @csrf
                        <h3>{{$product->name}}</h3>
                        <p class="price"><span class="mr-2 price-dc"
                                               style="font-size:25px;text-decoration-line: line-through;color: #4C5B5C">{{number_format($product->price)}}đ</span><span
                                class="price-sale" style="color: red">{{number_format($product->price_saleoff)}}đ</span><span
                                style="font-size: 15px"> (Tiết kiệm {{$product->discount}}%)</span></p>
                        <p>{!! $product->content !!}
                        </p>
                        <div class="row mt-4">
                            <div class="w-100"></div>
                            <div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	            		</span>
                                @if ($product->quantily>1)
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                    <span style="height: 40px;  line-height: 35px;  white-space: nowrap; color: black; margin-right: 5px">Số lượng</span>
                                    <input style="width: 70px; height: 35px" type="number" name="quantity" class=""
                                           value="1" min="1" max="{{ $product->quantily }}">
                                    <span class="input-group-btn ml-2"></span>
                                    <div>
                                        @if($errors->first('quantity'))
                                            <small id="emailHelp" class="form-text text-muted"><p
                                                    class="text-danger">{{ $errors->first('quantity') }}</p></small>
                                        @endif
                                    </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <p style="color: #000;">Số lượng còn: {{ $product->quantily }}</p>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Thêm vào giỏ hàng">
                        @else
                            <div class="col-md-12">
                                <p style="color: red;">Hết Hàng </p>
                            </div>
                        @endif
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
