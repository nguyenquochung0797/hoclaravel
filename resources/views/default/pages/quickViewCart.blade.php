<div class="modal fade" id="viewCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Giỏ hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cart-list">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr class="text-center">
                                    <th>&nbsp;</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Tổng tiềnl</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php  $total = 0; @endphp
                                @foreach($items as $item)
                                    <tr class="text-center">
                                        <td class="image-prod">
                                            <div class="img"
                                                 style="background-image:url({{$item->picture}});">
                                            </div>
                                        </td>
                                        <td class="product-name">
                                            <h3>{{$item->name}}</h3>
                                        <td class="price">{{number_format($item->price)}}đ</td>
                                        <td class="quantity">{{$item->quantity}}</td>
                                        <td class="total">{{number_format($item->total)}}đ</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
