@extends('default.layout')

@section('head')
    <title>Xác nhận đơn hàng</title>
@stop
@section('content')
    <section class="ftco-section ftco-cart">
        <div class="container">
            <h3 style="text-align: center;color: #761b18">Bạn đã xác nhận thành công</h3>
        </div>
    </section> <!-- .section -->
@stop
