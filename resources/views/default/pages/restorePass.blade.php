@extends('default.layout')
@section('content')
    <section style="width: 500px" class="get-in-touch">
        <h1 class="title">Khôi phục mật khẩu</h1>
        <form class="contact-form row"  method="POST" action="{{route('restorePass')}}">
            @csrf
            @php
              $custommer = \App\Custommer::find($id);
            @endphp
            <div class="form-field col-lg-12">
                <input class="input-text js-input" value="{{$custommer->username}}" type="username" name="username" readonly>
                <input class="input-text js-input" value="{{$custommer->id}}" type="hidden" name="id">
                <label class="label" for="name">Tài khoản</label>
            </div>
            <div class="form-field col-lg-12">
                <input class="input-text js-input" type="password" name="password">
                <label class="label" for="name">Mật khẩu mới</label>
                @if($errors->first('password'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('password') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12">
                <input  class="input-text js-input" name="password_confirm" type="password">
                <label class="label" for="email">Nhập lại mật khẩu</label>
                @if($errors->first('password_confirm'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('password_confirm') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12" style="text-align: center">
                <button id="btnSubmitSignIn" class="submit-btn">Đổi mật khẩu</button>
            </div>
        </form>
    </section>
@stop

