@extends('default.layout')
@section('content')
    <section style="width: 500px" class="get-in-touch">
        <h1 class="title">Khôi phục mật khẩu</h1>
        <form class="contact-form row"  method="POST" action="{{route('checkUsername')}}">
            @csrf
            <div class="form-field col-lg-12">
                <input id="name" class="input-text js-input" type="text" name="username">
                <label class="label" for="name">Username</label>
                @if($errors->first('username'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('username') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12">
                <input id="email" class="input-text js-input" name="email" type="email">
                <label class="label" for="email">Email</label>
                @if($errors->first('email'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('email') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12" style="text-align: center">
                <button id="btnSubmitSignIn" class="submit-btn">Gửi mail</button>
            </div>
        </form>
    </section>
@stop

