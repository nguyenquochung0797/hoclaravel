@extends('default.layout')
@section('content')
    <section class="get-in-touch">
        <h1 class="title">Đăng Ký</h1>
        @if(session()->has('signUp'))
            <div class="alert alert-success">
                {{ session()->get('signUp') }}
            </div>
        @endif
        <form class="contact-form row" id="form-SignUp" method="POST" action="{{route('storeSignUp')}}">
            @csrf
            <div class="form-field col-lg-6">
                <label class="label" for="name">Họ và tên</label>
                <input id="name" class="input-text js-input" type="text" name="name" value="{{old('name')}}">
                @if($errors->first('name'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('name') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-6">
                <label class="label" for="name">Tài Khoản</label>
                <input id="name" class="input-text js-input" type="text" name="username" value="{{old('username')}}">
                @if($errors->first('username'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('username') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-12 ">
                <input id="email" class="input-text js-input" name="email" type="email" value="{{old('email')}}">
                <label class="label" for="email">E-mail</label>
                @if($errors->first('email'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('email') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-6 ">
                <input id="company" class="input-text js-input" name="password" type="password">
                <label class="label" for="company">Mật Khẩu</label>
                @if($errors->first('password'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('password') }}</p></small>
                @endif
            </div>
            <div class="form-field col-lg-6 ">
                <input id="phone" class="input-text js-input" name="confirmPassword" type="password">
                <label class="label" for="phone">Xác nhận mật khẩu</label>
                @if($errors->first('confirmPassword'))
                    <small id="emailHelp" class="form-text text-muted"><p
                            class="text-danger">{{ $errors->first('confirmPassword') }}</p></small>
                @endif
            </div>
            <div class="form-group">
                <label for="captcha">Captcha</label>
                {!! NoCaptcha::renderJs() !!}
                {!! NoCaptcha::display() !!}
                <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
            </div>
            <div class="form-field col-lg-12">
                <button id="btnSubmitSignUp" class="submit-btn">Đăng Ký</button>
            </div>
        </form>
    </section>
@stop

