@php
    //session()->forget('wistList');
        if(session('wistList')!=null){
        $List = session()->get('wistList');
        $wishList = array_unique($List);
    }
@endphp

@if(session('wistList'))
    @foreach($wishList as $key => $value)
        @php
            $product = \App\Product::where('status',1)->where('id',$value)->first();
        @endphp
        <tr class="text-center">
            <td class="product-remove"><a data-id="{{$value}}" href="javascript:void()" id="delete_wishList"><span
                        class="ion-ios-close"></span></a></td>

            <td class="image-prod"><a
                    href="{{ route('detail',['slugProduct'=>$product->category->slug,'detail'=>$product->slug]) }}">
                    <div class="img" style="background-image:url({{$product->picture}});"></div>
                </a></td>

            <td class="product-name">
                <h3>
                    <a href="{{ route('detail',['items'=>3,'slugProduct'=>$product->category->slug,'detail'=>$product->slug]) }}">{{$product->name}}</a>
                </h3>
                <p>{!! $product->content !!}</p>
            </td>

            <td class="price">{{number_format($product->price_saleoff)}}đ</td>

        </tr><!-- END TR-->
    @endforeach
@endif

