<div class="row">
    @foreach($products as $product)
        @include('default.component.product-item',['items'=>4,'slug'=>$product->category,'product'=>$product])
    @endforeach
</div>
@if($total_page>1)
    <div class="row mt-5">
        <div class="col text-center">
            <div class="block-27">
                <ul>
                    @if($page_current>1)
                        <li><a href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$page_current-1}}">&lt;</a>
                        </li>@endif
                    @for($i=1;$i<=$total_page;$i++)
                        <li @if ($i==$page_current) class="active" @endif><span><a
                                    href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$i}}">{{$i}}</a></span></li>
                    @endfor
                    @if($page_current<$total_page)
                        <li><a href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$page_current+1}}">></a>
                        </li>@endif
                </ul>
            </div>
        </div>
    </div>
@endif
