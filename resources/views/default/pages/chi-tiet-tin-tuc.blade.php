@extends('default.layout')
@section('head')
    <title>{{$post->name}}</title>
@stop
@php
    $comments = \App\Comment::where('status',1)->orderBy('id','desc')->limit(5)->get();
@endphp
@section('content')

    <div class="hero-wrap hero-bread" style="background-image: url({{$post->picture}});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center banner-product" >
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <h2 class="mb-3" style="text-transform: uppercase">{{$post->name}}</h2>
                    {!! $post->content !!}
{{--                    <div class="tag-widget post-tag-container mb-5 mt-5">--}}
{{--                        <div class="tagcloud">--}}
{{--                            <a href="#" class="tag-cloud-link">Life</a>--}}
{{--                            <a href="#" class="tag-cloud-link">Sport</a>--}}
{{--                            <a href="#" class="tag-cloud-link">Tech</a>--}}
{{--                            <a href="#" class="tag-cloud-link">Travel</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}


                    <div class="pt-5 mt-5">
                        <h3 class="mb-5">{{count($comments)}} Bình luận</h3>
                        <ul class="comment-list">
                            @foreach($comments as $comment)
                                <li class="comment">
                                <div class="vcard bio">
                                    <img src="{{$comment->picture}}" alt="Image placeholder">
                                </div>
                                <div class="comment-body">
                                    <h3>{{$comment->name}}</h3>
                                    <div class="meta" style="color: #1a1a1a">{{$comment->created_at->format('d/m/Y-h:i A')}}</div>
                                    <p>{{$comment->content}}</p>
                                    <p><a href="#" class="reply">Trả lời</a></p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <!-- END comment-list -->

                        <div class="comment-form-wrap pt-5">
                            <h3 class="mb-5">Bình luận</h3>
                            <form action="{{route('storeComment',['slugCategory'=>$post->slug])}}"method="POST" class="p-5 bg-light">
                                @csrf
                                <input name="picture" type="hidden" value="/userfiles/images/custommer/user-custommer.png">
                                <div class="form-group">
                                    <label for="name">Họ và tên *</label>
                                    <input name="name" type="text" class="form-control" id="name" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input name="email" type="email" class="form-control"  value="{{old('email')}}">
                                    @if($errors->first('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="phone">Số điện thoại</label>
                                    <input name="phone" type="number" class="form-control"  value="{{old('phone')}}">
                                    @if($errors->first('phone'))
                                        <p class="text-danger">{{ $errors->first('phone') }}</p>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="message">Nội dung </label>
                                    <textarea name="contentCustommer" id="message" cols="30" rows="10" class="form-control">{{old('contentCustommer')}}</textarea>
                                    @if($errors->first('contentCustommer'))
                                        <p class="text-danger">{{ $errors->first('contentCustommer') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Gửi" class="btn py-3 px-4 btn-primary">
                                </div>

                            </form>
                        </div>
                    </div>
                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar ftco-animate">
                    <div class="sidebar-box">
                        <form action="#" class="search-form">
                            <div class="form-group">
                                <span class="icon ion-ios-search"></span>
                                <input type="text" class="form-control search" placeholder="Search...">
                            </div>
                        </form>
                    </div>
                    <div class="search-suggest" style="width: 500px"></div>
                </div>
            </div>
        </div>
    </section> <!-- .section -->

@stop
