@extends('default.layout')
@section('head')
    <title>Cung cấp thực phẩm lớn nhất Việt Nam</title>
@stop
@section('content')
    @php
        $banners = \App\Banner::where('status',1)->orderBy('order','desc')->limit(3)->get();
        $category = \App\Category::where('status',1)->orderBy('order','asc')->get();
        $featured_products = \App\Product::where('status',1)->where('classify',2)->get();
        $discount_products = \App\Product::where('status',1)->where('classify',1)->orderBy('updated_at','asc')->first();
        $comments = \App\Comment::where('status',1)->orderBy('id','desc')->limit(5)->get();
        $partners = \App\Partner::where('status',1)->orderBy('id','desc')->limit(5)->get();
    @endphp

    <section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            @foreach($banners as $banner)
                <div class="slider-item" style="background-image: url({{$banner->picture}});">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row slider-text justify-content-center align-items-center"
                             data-scrollax-parent="true">
                            <div class="col-md-12 ftco-animate text-center">
                                <h1 class="mb-2">{{$banner->link}}</h1>
                                <h2 class="subheading mb-4">Giá trị tích lũy niềm tin</h2>
                                <p><a href="#" class="btn btn-primary">View Details</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row no-gutters ftco-services">
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-shipped"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Miễn Phí Vận Chuyển</h3>
                            <span>Hoá đơn thanh toán trên 1,000,000đ</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-diet"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Thực Phẩm Tươi</h3>
                            <span>Sản phẩm luôn mới</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-award"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Chất lượng</h3>
                            <span>Sản phẩm chất lượng</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services mb-md-0 mb-4">
                        <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
                            <span class="flaticon-customer-service"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading">Hỗ trợ</h3>
                            <span>24/7 Hỗ trợ</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-category ftco-no-pt">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 order-md-last align-items-stretch d-flex">
                            <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex"
                                 style="background-image: url({{$category[0]->picture}});">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                                 style="background-image: url({{$category[1]->picture}});">
                                <div class="text px-3 py-1">
                                    <h2 class="mb-0"><a
                                            href="{{route('productDetail',['slugProduct'=>$category[1]->slug])}}">{{$category[1]->name}}</a>
                                    </h2>
                                </div>
                            </div>
                            <div class="category-wrap ftco-animate img d-flex align-items-end"
                                 style="background-image: url({{$category[2]->picture}});">
                                <div class="text px-3 py-1">
                                    <h2 class="mb-0"><a
                                            href="{{route('productDetail',['slugProduct'=>$category[2]->slug])}}">{{$category[2]->name}}</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                         style="background-image: url({{$category[3]->picture}});">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a
                                    href="{{route('productDetail',['slugProduct'=>$category[3]->slug])}}">{{$category[3]->name}}</a>
                            </h2>
                        </div>
                    </div>
                    <div class="category-wrap ftco-animate img d-flex align-items-end"
                         style="background-image: url({{$category[4]->picture}});">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a
                                    href="{{route('productDetail',['slugProduct'=>$category[4]->slug])}}">{{$category[4]->name}}</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Món Ăn Được Ưa Chuộng</span>
                    <h2 class="mb-4">NỔI BẬT</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($featured_products as $products)
                    @include('default.component.product-item',['items'=>3,'slug' => $products->category , 'product' => $products])
                @endforeach
            </div>
        </div>
    </section>

    <section class="ftco-section img" style="background-image: url({{$discount_products->picture}})">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-6 heading-section ftco-animate deal-of-the-day ftco-animate">
                    <span class="subheading">Giá tốt nhất cho bạn</span>
                    <h2 class="mb-4">Ưu đãi trong ngày</h2>
                    <h3><a href="{{route('productDetail',['slugProduct'=>$discount_products->slug])}}">{{$discount_products->name}}</a></h3>
                    <span class="price" style="text-decoration-line: line-through">{{number_format($discount_products->price)}}đ</span>
                    <span>{{number_format($discount_products->price_saleoff)}}đ</span>
                    <div id="timer" class="d-flex mt-5">
                        <div class="time pl-3" id="hours"></div>
                        <div class="time pl-3" id="minutes"></div>
                        <div class="time pl-3" id="seconds"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section testimony-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <span class="subheading">SỰ HÀI LÒNG</span>
                    <h2 class="mb-4"> Cảm Nhận Khách Hàng</h2>
                    <p>Tất cả vì khách hàng</p>
                </div>
            </div>
            <div class="row ftco-animate">
                <div class="col-md-12">
                    <div class="carousel-testimony owl-carousel">
                        @foreach($comments as $comment)
                            <div class="item">
                                <div class="testimony-wrap p-4 pb-5">
                                    <div class="user-img mb-5" style="background-image: url({{$comment->picture}})">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                    </div>
                                    <div class="text text-center">
                                        <p class="mb-5 pl-4 line" style="height: 70px">{{$comment->content}}</p>
                                        <p class="name">{{$comment->name}}</p>
                                        <span class="position">-----------------</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <hr>

    <section class="ftco-section ftco-partner">
        <div class="container">
            <div class="row">
                @foreach($partners as $partner)
                <div class="col-sm ftco-animate">
                    <a href="#" class="partner"><img src="{{$partner->picture}}" class="img-fluid"
                                                     alt="{{$partner->name}}"></a>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
        <div class="container py-4">
            <div class="row d-flex justify-content-center py-5">
                <div class="col-md-6">
                    <h2 style="font-size: 22px;" class="mb-0">Đăng kí để nhận được thông báo mới nhất</h2>
                    <span>Nhận thông tin cập nhật qua email về các cửa hàng mới nhất của chúng tôi và các ưu đãi đặc biệt</span>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <form action="#" class="subscribe-form">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Nhập email">
                            <input type="submit" value="Đăng kí" class="submit px-3">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
