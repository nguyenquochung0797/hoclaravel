@extends('default.layout')

@section('head')
    <title>Danh sách yêu thích</title>
@stop
@php
    //session()->forget('wistList');
        if(session('wistList')!=null){
        $List = session()->get('wistList');
        $wishList = array_unique($List);
    }
@endphp
@section('content')
    @include('default.component.banner-checkout',['banner'=>\App\Checkout::where('status',1)->where('slug','danh-sach-yeu-thich')->first()]);
    <section class="ftco-section ftco-cart">
        <div class="container">
            <h1 style="text-align: center;color: #761b18">Danh sách yêu thích</h1>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Hình sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                            </tr>
                            </thead>
                            <tbody id="delete_wish_List">
                            @if(session('wistList'))
                                @foreach($wishList as $key => $value)
                                @php
                                    $product = \App\Product::where('status',1)->where('id',$value)->first();
                                @endphp
                            <tr class="text-center">
                                <td class="product-remove"><a data-id="{{$value}}" href="javascript:void()" id="delete_wishList"><span class="ion-ios-close" ></span></a></td>

                                <td class="image-prod"><a href="{{ route('detail',['slugProduct'=>$product->category->slug,'detail'=>$product->slug]) }}"><div class="img" style="background-image:url({{$product->picture}});"></div></a></td>

                                <td class="product-name">
                                    <h3><a href="{{ route('detail',['items'=>3,'slugProduct'=>$product->category->slug,'detail'=>$product->slug]) }}">{{$product->name}}</a></h3>
                                    <p>{!! $product->content !!}</p>
                                </td>

                                <td class="price">{{number_format($product->price_saleoff)}}đ</td>

                            </tr><!-- END TR-->
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
