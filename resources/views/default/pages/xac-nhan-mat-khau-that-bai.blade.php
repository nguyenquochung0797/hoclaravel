@extends('default.layout')

@section('head')
    <title>Xác nhận mật khẩu</title>
@stop
@section('content')
    <section class="ftco-section ftco-cart">
        <div class="container">
            <h3 style="text-align: center;color: #761b18">Xác nhận thất bại</h3>
            <h4 style="text-align: center;">(Lưu ý: Có thể bạn đã vượt quá 60 phút để xác nhận đơn kể từ lúc nhận mail xác nhận)</h4>
        </div>
    </section> <!-- .section -->
@stop
