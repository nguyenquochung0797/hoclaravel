@extends('default.layout')
@section('head')
    <title>{{$product->name}}</title>
@stop
@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url({{$category->picture}});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center banner-product">
                    <h1 class="mb-0 bread">{{$category->name}}</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 ftco-animate">
                    <a href="" class="image-popup detailproduct">
                        <img class="xzoom img-fluid" id="xzoom-default" src="{{$product->picture}}"
                             xoriginal="{{$product->picture}}">
                    </a>
                    <div class="xzoom-thumbs" style="margin-top: 10px">
                        <a href="{{$product->picture}}">
                            <img class="xzoom-gallery" width="80" src="{{$product->picture}}"
                                 xpreview="{{$product->picture}}">
                        </a>
                        @foreach($picture_galler as $picture)
                            @if ($picture!='/no_image.jpg')
                            <a href="{{$picture}}">
                                <img class="xzoom-gallery" width="80" height="50" src="{{$picture}}">
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                    <form action="{{ route('addCart',['detail'=>$product->slug]) }}" method="POST">
                        @csrf
                        <h3>{{$product->name}}</h3>
                        <div class="rating d-flex">
                            <p class="text-left mr-4">
                                <a href="#" class="mr-2">5.0</a>
                                <a href="#"><span class="ion-ios-star-outline"></span></a>
                                <a href="#"><span class="ion-ios-star-outline"></span></a>
                                <a href="#"><span class="ion-ios-star-outline"></span></a>
                                <a href="#"><span class="ion-ios-star-outline"></span></a>
                                <a href="#"><span class="ion-ios-star-outline"></span></a>
                            </p>
                            <p class="text-left mr-4">
                                <a href="#" class="mr-2" style="color: #000;">1231 <span
                                        style="color: #bbb;">Đánh giá</span></a>
                            </p>
                            <p class="text-left">
                                <a href="#" class="mr-2" style="color: #000;">325 <span
                                        style="color: #bbb;">Đã mua</span></a>
                            </p>
                        </div>
                        <p class="price"><span class="mr-2 price-dc"
                                               style="font-size:25px;text-decoration-line: line-through;color: #4C5B5C">{{number_format($product->price)}}đ</span><span
                                class="price-sale" style="color: red">{{number_format($product->price_saleoff)}}đ</span><span
                                style="font-size: 15px"> (Tiết kiệm {{$product->discount}}%)</span></p>
                        <p>{!! $product->content !!}
                        </p>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                @php
                                    $size = [
                                        '0' => "Nhỏ",
                                        '1' => "Vừa",
                                        '2' => "Lớn",
                                    ];
                                @endphp
                                <div class="form-group d-flex">
                                    <div class="select-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="size" id="" class="form-control">
                                            @foreach($size as $k =>$v)
                                                <option value="{{$k}}">{{$v}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	            		</span>
                                @if ($product->quantily>1)
                                    <input type="hidden" name="id" value="{{$product->id}}">
                                    <span style="height: 40px;  line-height: 40px;  white-space: nowrap; color: black">Số lượng</span>
                                    <input style="width: 50px;" type="number" name="quantity" class="form-control "
                                           value="1" min="1" max="{{ $product->quantily }}">
                                    <span class="input-group-btn ml-2"></span>
                                    <div>
                                        @if($errors->first('quantity'))
                                            <small id="emailHelp" class="form-text text-muted"><p
                                                    class="text-danger">{{ $errors->first('quantity') }}</p></small>
                                        @endif
                                    </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-md-12">
                                <p style="color: #000;">Số lượng còn: {{ $product->quantily }}</p>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Thêm vào giỏ hàng">
                        @else
                            <div class="col-md-12">
                                <p style="color: red;">Hết Hàng </p>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <span class="subheading">Sản Phẩm</span>
                    <h2 class="mb-4">Sản Phẩm Liên Quan</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @php
                    $product_rela = \App\Product::where('status',1)->where('category_id',$product->category_id)->where('id','!=',$product->id)->get();
                @endphp
                @foreach($product_rela as $product)
                    @include('default.component.product-item',['items'=>3,'slug'=>$category,'product'=>$product])
                @endforeach
            </div>
        </div>
    </section>

@stop


