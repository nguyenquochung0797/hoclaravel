@extends('default.layout')

@section('head')
    <title>{{$title->name}}</title>
@stop
@section('content')

    <div class="hero-wrap hero-bread" style="background-image: url({{$slug->picture}});">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center banner-product">
                </div>
            </div>
        </div>
    </div>
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 product-ajax">
                    <div class="row">
                        @foreach($products as $product)
                            @include('default.component.product-item',['items'=>4,'slug'=>$product->category,'product'=>$product])
                        @endforeach
                    </div>
                    @if($total_page>1)
                        <div class="row mt-5">
                            <div class="col text-center">
                                <div class="block-27">
                                    <ul>
                                        @if($page_current>1)
                                            <li><a href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$page_current-1}}">&lt;</a>
                                            </li>@endif
                                        @for($i=1;$i<=$total_page;$i++)
                                            <li @if ($i==$page_current) class="active" @endif><span><a
                                                        href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$i}}">{{$i}}</a></span></li>
                                        @endfor
                                        @if($page_current<$total_page)
                                            <li><a href="{{route('productDetail',['slugProduct'=>$slugCategory])}}?page={{$page_current+1}}">></a>
                                            </li>@endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-4 sidebar ftco-animate">
                    <div class="row justify-content-center">
                        <ul class="product-category list-cate">
                            @foreach($categories as $category)
                                <li><a data-id="{{$category->id}}"
                                       data-link="{{route('productDetail',['slugProduct'=>$category->slug])}}"
                                       href="javascript:void()" id="subnav"
                                       class=""> {{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
