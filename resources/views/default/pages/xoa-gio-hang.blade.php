@if (isset($items))
    @php  $total = 0;@endphp
    @foreach($items as $item)
        <tr class="text-center">
            <td class="product-remove">
                <a data-id="{{$item->id}}" href="javascript:void()" id="delete_cart_item">
                    <span class="ion-ios-close"></span>
                </a>
                <input type="hidden" name="detail[{{$item->id}}][id_product]"
                       value="{{$item->id}}">
            </td>
            <td class="image-prod">
                <div class="img"
                     style="background-image:url({{$item['conditions']['picture']}});"></div>
                <input type="hidden" name="detail[{{$item->id}}][picture_product]"
                       value="{{$item['conditions']['picture']}}">
            </td>

            <td class="product-name">
                <h3>{{$item->name}}</h3>
                {{--                                                 <p>{!! $item['conditions']['content'] !!}</p>--}}
                <input type="hidden" name="detail[{{$item->id}}][name_product]"
                       value="{{$item->name}}">
            </td>

            <td class="price">{{number_format($item->price)}}đ</td>
            <input type="hidden" name="detail[{{$item->id}}][price_product]"
                   value="{{$item->price}}">

            <td class="quantity">
                <div class="input-group mb-3">
                    <input type="text" name="quantity"
                           class="quantity form-control input-number"
                           value="{{$item->quantity}}" readonly>
                    <input type="hidden" name="detail[{{$item->id}}][quantity_product]"
                           value="{{$item->quantity}}">
                </div>
            </td>

            <td class="total">{{number_format(\Cart::get($item->id)->getPriceSum())}}đ
            </td>
            <input type="hidden" name="detail[{{$item->id}}][total_product]"
                   value="{{\Cart::get($item->id)->getPriceSum()}}">
        </tr>
        @php $total += \Cart::get($item->id)->getPriceSum(); @endphp
        <input type="hidden" name="total"
               value="{{$total}}">
    @endforeach
@endif
@if (isset($items) && count($items)>=1)
    <tr>
        <td>
            <a href="javascript:void()" id="delete_cart_all"><input type="button" class="btn btn-danger"
                                                                    value="Xoá giỏ hàng"></a>
        </td>
    </tr>
@else
    <tr>
        <td>
            <h3>Giỏ hàng rỗng</h3>
        </td>
    </tr>
@endif
