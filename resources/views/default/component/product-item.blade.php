<div class="col-md-6 col-lg-{{$items}} ">
    <div class="product">
        <a href="{{route('detail',['slugProduct'=>$slug->slug,'detail'=>$product->slug])}}" class="img-prod">
            <div class="discount">
                <img class="img-fluid" src="{{$product->picture}}"
                     alt="Colorlib Template">
                <span class="status">{{$product->discount}}%</span>
            </div>
            <div class="overlay"></div>
        </a>
        <div class="text py-3 pb-4 px-3 text-center">
            <h3>
                <a href="{{route('detail',['slugProduct'=>$slug->slug,'detail'=>$product->slug])}}">{{$product->name}}</a>
            </h3>
            @if ($product->quantily>1)

                <div class="d-flex">
                    <div class="pricing">
                        <p class="price"><span class="mr-2 price-dc">{{number_format($product->price)}}đ</span><span
                                class="price-sale">{{number_format($product->price_saleoff)}}đ</span>
                        </p>
                    </div>
                </div>

                <div class="bottom-area d-flex px-3">
                    <div class="m-auto d-flex">
                        <a href="javascript:void()" data-id="{{$product->id}}" data-toggle="modal"
                           data-target="#modalProductQuickView{{$product->id}}"
                           class="add-to-cart d-flex justify-content-center align-items-center text-center quick_view">
                            <span><i class="ion-ios-eye"></i></span>
                        </a>
                        <form action="#" method="POST" id="form_data">
                            @csrf
                            <input type="hidden" value="1" name="quantity">
                        </form>
                        @php
                            $items = \Cart::session('bill')->getContent();
                            $style = "";
                            if($items!=null){
                               foreach($items as $item){
                                  if($item->id==$product->id)$style ="style=background:cadetblue";
                               }
                            }
                        @endphp
                        <div class="addCartQuick_{{$product->id}}">
                        <a {{$style}} data-id="{{$product->id}}" id="addCart" href="javascript:void()" class="buy-now d-flex justify-content-center align-items-center mx-1">
                            <span><i class="ion-ios-cart"></i></span>
                        </a>
                        </div>
                        @php
                            $style = "";
                            if(session('wistList')!=null){
                               $List = session()->get('wistList');
                               $wishList = array_unique($List);
                               foreach($wishList as $key =>$value){
                                   if($value==$product->id)$style = "style=background:red";
                               }
                            }
                        @endphp
                        <div class="wishList_{{$product->id}}">
                            <a {{$style}} data-id="{{$product->id}}" id="wishList" href="javascript:void()"
                               class="heart d-flex justify-content-center align-items-center">
                                <span><i class="ion-ios-heart abc"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-12">
                    <p style="color: red;">Hết Hàng </p>
                </div>
            @endif
        </div>
    </div>
</div>


