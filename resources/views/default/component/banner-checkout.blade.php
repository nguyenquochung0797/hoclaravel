<div class="hero-wrap hero-bread" style="background-image: url({{$banner->picture}}">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center banner-product">
            </div>
        </div>
    </div>
</div>
