<!DOCTYPE html>
<html>
<head>
    @include("admin.template.head")
</head>
<body class="skin-blue">
    @include("admin.template.header")
<div class="wrapper row-offcanvas row-offcanvas-left">
    <aside class="left-side sidebar-offcanvas">
        @include("admin.template.sidebar")
    </aside>
    <aside class="right-side">
        @include("admin.template.content_header")
        <section class="content">
            @yield('content')
        </section>
    </aside>
</div>
    @include("admin.template.script")
</body>
</html>
@yield('scripts')
