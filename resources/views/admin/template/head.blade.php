<meta charset="UTF-8">
<title>Admin</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="{{ asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="{{ asset('admin/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
<!-- Ionicons -->
<link href="{{ asset('admin/css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="{{ asset('admin/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/css/css.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
