
<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left info">
            <p>Hello, {{ Auth::user()->name }}</p>
            <i class="fa fa-circle text-success"></i> Online
        </div>
    </div>
    <ul class="sidebar-menu">
        <li class="treeview">
            <a href="{{ route("admin.category.index") }}">
                <i class="fa fa-bar-chart-o"></i>
                <span>Categories</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.product.index") }}">
                <i class="fa fa-cutlery"></i>
                <span>Products</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.post.index") }}">
                <i class="fa fa-laptop"></i>
                <span>Posts</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.bill.index") }}">
                <i class="fa fa-money"></i>
                <span>Bill</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.code.index") }}">
                <i class="fa fa-tags"></i>
                <span>Codes</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.ship.index") }}">
                <i class="fa fa-flash"></i>
                <span>Ship</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.tag.index") }}">
                <i class="fa fa-tags"></i>
                <span>Tags</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.user.index") }}">
                <i class="fa fa-users"></i>
                <span>Users</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.role.index") }}">
                <i class="fa fa-wrench"></i>
                <span>Roles</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.banner.index") }}">
                <i class="fa fa-flash"></i>
                <span>Banners</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.comment.index") }}">
                <i class="fa fa-comment-o"></i>
                <span>Comments</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.contact.index") }}">
                <i class="fa fa-phone"></i>
                <span>Contact</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.info.index") }}">
                <i class="fa fa-info-circle"></i>
                <span>Info</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.menu.index") }}">
                <i class="fa fa-th-list"></i>
                <span>Menu</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.policy.index") }}">
                <i class="fa fa-tags"></i>
                <span>Policy</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.partner.index") }}">
                <i class="fa fa-puzzle-piece"></i>
                <span>Partners</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route("admin.checkout.index") }}">
                <i class="fa fa-credit-card"></i>
                <span>Checkout</span>
            </a>
        </li>
    </ul>
</section>

