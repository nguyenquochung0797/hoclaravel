@php
    use App\Http\Helper\Common;
    $table = 'banner';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'update') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>

                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <label class="control-label" for="">Upload image</label>
                                    <div class="action_image">
                                        <button type="button" class="btn btn-info btn_upload">Upload</button>
                                        <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                    </div>
                                    <input type="hidden" name="id" type="text" class="form-control" value="{{$items->id}}">
                                    <div class="preview_image">
                                        <img src="{{(old('picture_url')==null)?$items->picture:(old('picture_url'))}}" style="max-width: 200px">
                                        <input type="hidden" name="picture_url" value="{{$items->picture}}">
                                    </div>
                                    @if($errors->first('picture_url'))
                                        <p class="text-danger">{{ $errors->first('picture_url') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" id="status" class="form-control" >
                                        @php
                                            $fillter_status = [
                                                    '-1' => '-- Chọn trạng thái --',
                                                    '1' => 'Kích hoạt',
                                                    '0' => 'Không kích hoạt',
                                            ];
                                        $value = (old('status')==null)?$items->status:(old('status'));
                                        @endphp
                                        @foreach($fillter_status as $k => $v)
                                        <option @if($k == $value) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('status'))
                                        <p class="text-danger">{{ $errors->first('status') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Link</label>
                                    <input name="link" type="text" class="form-control" value="{{(old('link')==null)?$items->link:(old('link'))}}">
                                    @if($errors->first('link'))
                                        <p class="text-danger">{{ $errors->first('link') }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    @include('ckfinder::setup')

@stop
@section('scripts')
    <script>
        var btnUpload = $(".btn_upload");
        var btnRemove= $(".btn_remove");
        btnUpload.on('click', function(){
            selectFileWithCKFinder(this);
        })
        btnRemove.on('click', function(){
            remmoveIMG(this);
        })
        function remmoveIMG(t) {
            $(t).parents(".form-group").children(".preview_image").children("img").attr("src",'/no_image.jpg');
            $(t).parents(".form-group").children(".preview_image").children("input").val('');

        }
        function selectFileWithCKFinder( t) {
            CKFinder.popup( {
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var urlFile = file.getUrl();

                        $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                        $(t).parents(".form-group").children(".preview_image").children("input").val(urlFile);
                    } );

                    finder.on( 'file:choose:resizedImage', function( evt ) {

                    } );
                }
            } );
        }
    </script>
@stop
