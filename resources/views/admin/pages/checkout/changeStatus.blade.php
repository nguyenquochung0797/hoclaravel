@php
    $color_active = ($item->status==0)?"black":"chartreuse";
@endphp
<i  id="edit" data-menu="{{$title}}" class='fa fa-fw fa-check-circle'  style="color:{{$color_active}}" data-id="{{$item->id}}" data-status="{{$item->status}}">
</i>
