@php
    use App\Http\Helper\Common;
    $table = 'menu';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'store') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Save
                    </a>
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <label for="">Tên quyền<span class="text-danger">*</span></label>
                                    <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
                                    @if($errors->first('name'))
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" id="" class="form-control">
                                        @php
                                            $status = [
                                                '-1' => '-- Chọn trạng thái --',
                                                '1' => 'Kích hoạt',
                                                '0' => 'Không kích hoạt',
                                            ];
                                            $stt = (old('status')==null)?-1:(old('status'));
                                        @endphp
                                        @foreach($status as $key => $value)
                                            <option @if($key == $stt) selected @endif value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('status'))
                                        <p class="text-danger">{{ $errors->first('status') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Slug</label>
                                    <input id="slug" name="slug" type="text" class="form-control" value="{{ old('slug') }}">
                                    @if($errors->first('slug'))
                                        <p class="text-danger">{{ $errors->first('slug') }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>


@stop
