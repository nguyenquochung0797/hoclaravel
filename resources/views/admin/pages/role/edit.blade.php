@php
    use App\Http\Helper\Common;
    $table = 'role';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'update') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>

                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                            <li class=""><a href="#meta_tab" data-toggle="tab">Quyền</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <input type="hidden" name="id" type="text" class="form-control" value="{{$items->id}}">
                                    <label for="">Tên bài viết <span class="text-danger">*</span></label>
                                    <input id="name" name="name" type="text" class="form-control" value="{{(old('name')==null)?$items->name:(old('name'))}}">
                                    @if($errors->first('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" id="status" class="form-control" >
                                        @php
                                            $fillter_status = [
                                                    '-1' => '-- Chọn trạng thái --',
                                                    '1' => 'Kích hoạt',
                                                    '0' => 'Không kích hoạt',
                                            ];
                                        $value = (old('status')==null)?$items->status:(old('status'));
                                        @endphp
                                        @foreach($fillter_status as $k => $v)
                                        <option @if($k == $value) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                </div>
                        </div>
                            <div class="tab-pane" id="meta_tab">
                                <div class="form-group">
                                    <div class="form-group">
                                        <h3>Quyền</h3>
                                        <input type="checkbox" id="checkall" >
                                        <label for="">Tất cả</label>
                                        <hr>
                                        @php
                                            $permissions = config('permissions');
                                            $perkey = json_decode($items->permissions,true);
                                            $perarr = [];
                                            foreach ($perkey as $key => $value){
                                                    array_push($perarr,$key);
                                            }

                                            $oldkey = (old('permissions')!=null)?old('permissions'):[];
                                            $oldarr = [];
                                            foreach ( $oldkey as $key => $value){
                                                    array_push($oldarr,$key);
                                            }
                                            $check = ($oldarr==null)?$perarr:$oldarr;
                                        @endphp
                                        @foreach($permissions as $k => $per)
                                            <div class="per_group">
                                                <h4>{{ ucfirst($k) }}</h4>
                                                @foreach($per as $action => $label)
                                                    @php
                                                        $keyPermision = $k . "." . $action;
                                                    @endphp
                                                    <div>
                                                        <input @if(in_array($keyPermision,$check))  checked @endif class="checkClass"  name="permissions[{{$keyPermision}}]" type="checkbox"  value="true">
                                                        <label for="">{{ $label }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                    </div>
                </form>
            </div>

        </div>
    </div>


@stop
