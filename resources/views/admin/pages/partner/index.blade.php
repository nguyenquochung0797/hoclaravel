@php
    use App\Http\Helper\Common;
    $table = 'partner';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a href="{{ route($href.'index') }}"class="btn btn-app">
                        <i class="fa fa-undo"></i> Reload
                    </a>
                    <a href="{{ route($href.'add') }}" class="btn btn-app">
                        <i class="fa fa-plus"></i> Add
                    </a>
                    <a href="{{ route($href.'trash') }}" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Trash
                    </a>
                    <a data-link="{{ route($href.'removeMulti') }}" href="javascript:void()" onclick="deleleMulti(this)" class="btn btn-app">
                        <i class="fa fa-times-circle"></i> Delete
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box" >
                <div class="box-header">
                    <h2 class="box-title">{{ucfirst($title)}}</h2>
                </div>
                <div class="box-body">
                    <form class="a" action="{{route($href.'index')}}">
                        <div class="input-group input-group-sm">
                            <input  type="search" class="form-control" name="query" placeholder="Nhập nội dung...." value="@if(isset($_GET['query'])!=null) {{$_GET['query']}} @endif">
                            <span class="input-group-btn">
                                 <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                            <span class="input-group-btn">
                            <a class="btn btn-flat btn-warning" href="{{route($href.'index')}}"><i class="fa fa-refresh"></i></a>
                            </span>
                        </div>
                    </form>
                    <div class="a">
                       <div class="input-group input-group-sm">
                           <select name="status" id="filter_status" class="form-control" >
                               @php
                                   $fillter_status = [
                                           '' => 'Tất cả',
                                           '1' => 'Kích hoạt',
                                           '0' => 'Không kích hoạt',
                                   ]
                               @endphp
                               @foreach($fillter_status as $k => $v)
                                   <option @if($params['fillter_status']== $k) selected @endif value="{{ $k }}">{{ $v }}</option>
                               @endforeach
                           </select>
                       </div>
                   </div>
                    <form action="#" method="POST" id="form_data">
                        @csrf
                    <table class="table table-bordered dataTable" >
                        <tbody>
                        <thead>
                            <thead>
                                <th><input type="checkbox" id="checkall" ></th>
                                <th style="width: 10px">STT</th>
                                {!! Common::createThead("Name", "name", $params) !!}
                                {!! Common::createThead("Status", "status", $params) !!}
                                <th>Picture</th>
                                <th>Ordering</th>
                                <th>Created_at </th>
                                <th>Updated_at</th>
                                <th><a class="btn btn-info" href="{{ route($href.'restore') }}">
                                        <i class="fa fa-repeat"></i>
                                    </a>
                                </th>
                            </thead>
                        </tr>
                        @php
                            $i=1;
                        @endphp
                            @foreach($items as $item)
                                <tr id="id{{$item->id}}">
                                    <td><input type="checkbox" class="checkClass" name="cid[]" value="{{$item->id}}"></td>
                                    <td>{{$i++}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @php
                                            $color_active = ($item->status==0)?"black":"chartreuse";
                                        @endphp
                                        <a href="javascript:void()" id="changeStatus_{{$item->id}}">
                                            <i  id="edit" data-menu="{{$title}}" class='fa fa-fw fa-check-circle'  style="color:{{$color_active}}" data-id="{{$item->id}}" data-status="{{$item->status}}">
                                            </i>
                                        </a>
                                    </td>
                                    <td>
                                        <img style="max-width: 60px " src="{{$item->picture}}" alt="">
                                    </td>
                                    <td><input data-menu="{{$title}}" data-id="{{$item->id}}" class="order" type="number" value="{{$item->order}}"></td>
                                    <td>{{$item->created_at->format('d/m/Y-h:i A')}}</td>
                                    <td>{{$item->updated_at->format('d/m/Y-h:i A')}}</td>
                                    <td><a class="btn btn-warning" href="{{route('admin.'.$table.'.edit',['id'=>$item->id])}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a onclick="return Del()" class="btn btn-danger"
                                           href="{{route('admin.'.$table.'.remove',['id'=>$item->id])}}">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </form>
                </div>
                <div class="box-footer clearfix pull-right">
                    {{$items->appends( request()->input() )->links() }}
                </div>
            </div><!-- /.box -->

        </div>
    </div>


@stop
