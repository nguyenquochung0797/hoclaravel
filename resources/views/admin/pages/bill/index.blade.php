@php
    use App\Http\Helper\Common;
    $table = 'bill';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a href="{{ route($href.'index') }}" class="btn btn-app">
                        <i class="fa fa-undo"></i> Reload
                    </a>
                    <a href="{{ route($href.'trash') }}" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Trash
                    </a>
                    <a data-link="{{ route($href.'removeMulti') }}" href="javascript:void()" onclick="deleleMulti(this)"
                       class="btn btn-app">
                        <i class="fa fa-times-circle"></i> Delete
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">{{ucfirst($title)}}</h2>
                </div>
                <div class="box-body">
                    <form class="a" action="{{route($href.'index')}}">
                        <div class="input-group input-group-sm">
                            <input type="search" class="form-control" name="query" placeholder="Nhập nội dung...."
                                   value="@if(isset($_GET['query'])!=null) {{$_GET['query']}} @endif">
                            <span class="input-group-btn">
                                 <button class="btn btn-success btn-flat" type="submit"><i
                                         class="fa fa-search"></i></button>
                            </span>
                            <span class="input-group-btn">
                            <a class="btn btn-flat btn-warning" href="{{route($href.'index')}}"><i
                                    class="fa fa-refresh"></i></a>
                            </span>
                        </div>
                    </form>
                    <div class="a">
                        <div class="input-group input-group-sm">
                            <select name="status" id="filter_status" class="form-control">
                                @php
                                    $fillter_status = [
                                            '' => 'Tất cả',
                                            '0' => 'Đang chờ xác nhận',
                                            '1' => 'Đang vận chuyển',
                                            '2' => 'Đã hoàn thành',
                                            '3' => 'Đã huỷ đơn',
                                    ]
                                @endphp
                                @foreach($fillter_status as $k => $v)
                                    <option @if($params['fillter_status']== $k) selected
                                            @endif value="{{ $k }}">{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <form action="#" method="POST" id="form_data">
                        @csrf
                        <table class="table table-bordered dataTable">
                            <tbody>
                            <thead>
                            <thead>
                            <th><input type="checkbox" id="checkall"></th>
                            <th style="width: 10px">STT</th>
                            {!! Common::createThead("Name", "name", $params) !!}
                            <th>Phone</th>
                            <th>Total</th>
                            {!! Common::createThead("Status", "status", $params) !!}
                            <th>Created_at</th>
                            <th>Updated_at</th>
                            <th><a class="btn btn-info" href="{{ route($href.'restore') }}">
                                    <i class="fa fa-repeat"></i>
                                </a>
                            </th>
                            </thead>
                            </tr>
                            @php
                                $i=1;
                            @endphp
                            @foreach($items as $item)
                                <tr id="id{{$item->id}}">
                                    <td><input type="checkbox" class="checkClass" name="cid[]" value="{{$item->id}}">
                                    </td>
                                    <td>{{$i++}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{number_format($item->totalBill)}}đ</td>
                                    <td>
                                        @php
                                            $icon_status = [
                                                    '0' => 'fa-exclamation',
                                                    '1' => 'fa-truck',
                                                    '2' => 'fa-check-circle',
                                                    '3' => 'fa-times',
                                            ];
                                            $color_status = [
                                                    '0' => 'black',
                                                    '1' => 'blue',
                                                    '2' => 'green',
                                                    '3' => 'red',
                                            ];
                                        @endphp

                                        <a href="{{route('admin.'.$table.'.changeStatus',['id'=>$item->id])}}"><i
                                                style="font-size: 20px; color: @foreach($color_status as $key => $value) @if ($key==$item->status){{$value}}@endif @endforeach"
                                                class="edit fa fa-fw @foreach($icon_status as $key => $value) @if ($key==$item->status){{$value}}@endif @endforeach "></i></a>

                                    </td>
                                    <td>{{$item->created_at->format('d/m/Y-h:i A')}}</td>
                                    <td>{{$item->updated_at->format('d/m/Y-h:i A')}}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{route('admin.'.$table.'.info',['id'=>$item->id])}}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="btn btn-warning" href="{{route('admin.'.$table.'.edit',['id'=>$item->id])}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-primary"
                                           href="{{ route('admin.bill.changeBill',['id'=>$item->id]) }}">
                                            <i class="fa fa-eraser"></i>
                                        </a>
                                        <a onclick="return Del()" class="btn btn-danger"
                                           href="{{route('admin.'.$table.'.remove',['id'=>$item->id])}}">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                        </table>
                    </form>
                </div>
                <div class="box-footer clearfix pull-right">
                    {{$items->appends( request()->input() )->links() }}
                </div>
            </div><!-- /.box -->

        </div>
    </div>


@stop
