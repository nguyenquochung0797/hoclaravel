@php
    use App\Http\Helper\Common;
    $table = 'bill';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin hoá đơn</a></li>
                        <li class=""><a href="#meta_tab" data-toggle="tab">Chi tiết hoá đơn</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general_tab">
                            <div class="row">
                                <div class="col-lg-2" style="font-size: 25px">
                                    <div class="form-group">
                                        <span class="label label-info">Họ và tên </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Số điện thoại </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Email </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Địa chỉ </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Tổng tiền </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Trạng thái</span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-info">Thời gian lập hoá đơn</span>
                                    </div>
                                </div>
                                <div class="col-lg-10" style="font-size: 25px">
                                    <div class="form-group">
                                        <span class="label label-default">{{$items->name}}</span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-default">{{$items->phone}}</label>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-default">{{$items->email}}</span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-default">{{$items->address}}</span>
                                    </div>
                                    <div class="form-group">
                                        <span class="label label-default">{{number_format($items->totalBill)}}đ</span>
                                    </div>
                                    <div class="form-group">
                                        @php
                                            $fillter_status = [
                                            '' => 'Tất cả',
                                            '0' => 'Đang chờ xác nhận',
                                            '1' => 'Đang vận chuyển',
                                            '2' => 'Đã hoàn thành',
                                            '3' => 'Đã huỷ đơn',
                                    ]
                                        @endphp
                                        @foreach($fillter_status as $k => $v)
                                            @if($k == $items->status) <span
                                                class="label label-default">{{ $v }}</span> @endif
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <span
                                            class="label label-default">{{$items->created_at->format('d/m/Y-h:i A')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="meta_tab">
                            <section class="ftco-section ftco-cart">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 ftco-animate">
                                            <div class="cart-list">
                                                <table class="table">
                                                    <thead class="thead-primary">
                                                    <tr class="text-center">
                                                        <th>STT</th>
                                                        <th>Hình ảnh</th>
                                                        <th>Tên sản phẩm</th>
                                                        <th>Giá</th>
                                                        <th>Số lượng</th>
                                                        <th>Tổng tiềnl</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i =1 ; @endphp
                                                    @foreach($detail as $item)
                                                        <tr class="text-center">
                                                            <td class="id">{{$i++}}</td>
                                                            <td class="image-prod">
                                                                <img style="width: 90px;height: 50px"
                                                                     src="{{$item->picture}}"
                                                                     alt="">
                                                            </td>
                                                            <td class="price">{{$item->name}}</td>
                                                            <td class="price">{{number_format($item->price)}}đ</td>
                                                            <td class="price">{{$item->quantity}}</td>
                                                            <td class="price">{{number_format($item->total)}}đ</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop

