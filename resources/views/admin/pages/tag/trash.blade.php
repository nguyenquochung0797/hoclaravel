@php
    use App\Http\Helper\Common;
    $table = 'tag';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a href="{{ route($href.'index') }}" class="btn btn-app">
                        <i class="fa fa-undo"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">Bordered Table</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" >
                        <tbody>
                        <tr>
                            <th style="width: 10px">STT</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Picture</th>
                            <th>Slug</th>
                            <th>Created_at </th>
                            <th>Updated_at</th>
                        </tr>
                        @php
                            $i=1;
                        @endphp
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @if($item->status==1)
                                            <a href="/admin/{{$table}}/changeStatus/{{$item->id}}"><i style="font-size: 20px; color: chartreuse" class="edit fa fa-fw fa-check-circle"></i></a>
                                        @else
                                            <a href="/admin/{{$table}}/changeStatus/{{$item->id}}"><i style="font-size: 20px; color: black"class="edit fa fa-fw fa-check-circle"></i></a>
                                        @endif
                                    </td>
                                    <td>
                                        <img src="/storage/small/{{$item->picture}}" alt="">
                                    </td>
                                    <td>{{$item->slug}}</td>
                                    <td>{{$item->created_at->format('d/m/Y-h:i:s A')}}</td>
                                    <td>{{$item->updated_at->format('d/m/Y-h:i:s A')}}</td>
                                    <td><a class="btn btn-info" href="/admin/{{$table}}/restoreID/{{$item->id}}">
                                            <i class="fa fa-repeat"></i>
                                        </a>
                                        <a onclick="return Del()" class="btn btn-danger" href="/admin/{{$table}}/removetrash/{{$item->id}}">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix pull-right">
                    {{$items->links()}}
                </div>
            </div><!-- /.box -->

        </div>
    </div>


@stop
