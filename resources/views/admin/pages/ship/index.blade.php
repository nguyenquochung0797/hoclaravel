@php
    use App\Http\Helper\Common;
    $table = 'ship';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a href="{{ route($href.'index') }}"class="btn btn-app">
                        <i class="fa fa-undo"></i> Reload
                    </a>
                    <a href="{{ route($href.'update') }}" class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box" >
                <div class="box-body">
                    <form action="#" method="POST" id="form_data">
                        @csrf
                    <table class="table table-bordered dataTable" >
                        <tbody>
                        <thead>
                            <thead>
                                <th style="width: 10px">STT</th>
                                {!! Common::createThead("Name", "name", $params) !!}
                            </thead>
                        </tr>
                        @php
                            $i=1;
                        @endphp
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td><a data-id="{{$item->id}}" href="javascript:void()" class="province_name">{{$item->_name}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </form>
                    <div class="paginate" style="text-align: center">
                        {{$items->appends( request()->input() )->links() }}
                    </div>
                </div>
            </div><!-- /.box -->
        </div>
        <div class="col-md-6" id="district_list">

        </div>
    </div>


@stop
