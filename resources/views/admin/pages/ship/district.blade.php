<div class="box">
    <div class="box-body">
        <input type="text" value="" id="price_province">
        <button id="btn-province">Tỉnh</button>
        <input type="text" value="" id="price_all">
        <button id="btn-all">Tất Cả</button>
        <form action="#" method="POST" id="form_data">
            @csrf
            <table class="table table-bordered dataTable">
                <tbody>
                <thead>
                <thead>
                <th style="width: 10px">STT</th>
                <th>Name</th>
                <th>Price Ship</th>
                </thead>
                </tr>
                @php
                    $i=1;
                @endphp
                @foreach($items as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$item->_name}}</td>
                        <td><input data-id="{{$item->id}}" type="number" value="{{$item->priceShip}}" id="price_Ship">
                        </td>
                        <input type="hidden" value="{{$item->_province_id}}" id="id_province">
                    </tr>
                @endforeach
                    </tbody>
            </table>
        </form>
    </div>
</div><!-- /.box -->

