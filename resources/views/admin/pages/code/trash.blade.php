@php
    use App\Http\Helper\Common;
    $table = 'code';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a href="{{ route($href.'index') }}" class="btn btn-app">
                        <i class="fa fa-undo"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box" >
                <div class="box-header">
                    <h3 class="box-title">{{ucfirst($title)}}</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" >
                        <tbody>
                        <tr>
                            <th style="width: 10px">STT</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Code</th>
                            <th>Created_at </th>
                            <th>Updated_at</th>
                        </tr>
                        @php
                            $i=1;
                        @endphp
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @php
                                            $color_active = ($item->status==0)?"black":"chartreuse";
                                        @endphp
                                        <a href="javascript:void()" id="changeStatus_{{$item->id}}">
                                            <i  id="edit" data-menu="{{$title}}" class='fa fa-fw fa-check-circle'  style="color:{{$color_active}}" data-id="{{$item->id}}" data-status="{{$item->status}}">
                                            </i>
                                        </a>
                                    </td>
                                    <td>{{$item->code}}</td>
                                    <td>{{$item->created_at->format('d/m/Y-h:i:s A')}}</td>
                                    <td>{{$item->updated_at->format('d/m/Y-h:i:s A')}}</td>
                                    <td><a class="btn btn-info" href="{{route('admin.'.$table.'.restoreID',['id'=>$item->id])}}">
                                            <i class="fa fa-repeat"></i>
                                        </a>
                                        <a onclick="return Del()" class="btn btn-danger" href="{{route('admin.'.$table.'.removetrash',['id'=>$item->id])}}">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix pull-right">
                    {{$items->links()}}
                </div>
            </div><!-- /.box -->

        </div>
    </div>


@stop
