@php
    use App\Http\Helper\Common;
    $table = 'code';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'update') }}" href="javascript:void()" onclick="submitForm(this)"
                       class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>

                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <input type="hidden" name="id" type="text" class="form-control"
                                           value="{{$items->id}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Tên giảm giá<span class="text-danger">*</span></label>
                                    <input id="name" name="name" type="text" class="form-control"
                                           value="{{$items->name??old('name')}}">
                                    @if($errors->first('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Mã giảm giá<span class="text-danger">*</span></label>
                                    <input id="code" name="code" type="text" class="form-control"
                                           value="{{$items->code??old('code')}}">
                                    @if($errors->first('code'))
                                        <p class="text-danger">{{ $errors->first('code') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Discount<span class="text-danger">*</span></label>
                                    <div class="form-group">
                                        <label><input @if ($items->discount_type=='percent') checked @endif type="radio"
                                                      name="discount_type" value="percent">(%)</label>
                                        <label><input @if ($items->discount_type=='VND') checked @endif type="radio"
                                                      name="discount_type" value="VND">(VND)</label>
                                    </div>
                                    <input id="discount" name="discount" type="number" class="form-control"
                                           value="@if ($items->discount_type=='percent'){{$items->discount_percent}}@elseif ($items->discount_type=='VND'){{$items->discount_VND}}@else {{old('discount')}}@endif">
                                    @if($errors->first('discount'))
                                        <p class="text-danger">{{ $errors->first('discount') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Ngày hết hạn<span class="text-danger">*</span></label>
                                    <input id="date" name="date" type="datetime-local" class="form-control"
                                           value="{{$items->expiry_date->format("d/m/Y-h:i A")??old('date')}}">
                                    @if($errors->first('date'))
                                        <p class="text-danger">{{ $errors->first('date') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" id="status" class="form-control">
                                        @php
                                            $fillter_status = [
                                                    '-1' => '-- Chọn trạng thái --',
                                                    '1' => 'Kích hoạt',
                                                    '0' => 'Không kích hoạt',
                                            ];
                                        $value = (old('status')==null)?$items->status:(old('status'));
                                        @endphp
                                        @foreach($fillter_status as $k => $v)
                                            <option @if($k == $value) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('status'))
                                        <p class="text-danger">{{ $errors->first('status') }}</p>
                                    @endif
                                </div>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div>
                </form>
            </div>

        </div>
    </div>

    @include('ckfinder::setup')
@stop
@section('scripts')
    <script>
        var btnUpload = $(".btn_upload");
        var btnRemove = $(".btn_remove");
        btnUpload.on('click', function () {
            selectFileWithCKFinder(this);
        })
        btnRemove.on('click', function () {
            remmoveIMG(this);
        })

        function remmoveIMG(t) {
            $(t).parents(".form-group").children(".preview_image").children("img").attr("src", '/no_image.jpg');
            $(t).parents(".form-group").children(".preview_image").children("input").val('');

        }

        function selectFileWithCKFinder(t) {
            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        var urlFile = file.getUrl();

                        $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                        $(t).parents(".form-group").children(".preview_image").children("input").val(urlFile);
                    });

                    finder.on('file:choose:resizedImage', function (evt) {

                    });
                }
            });
        }
    </script>
@stop
