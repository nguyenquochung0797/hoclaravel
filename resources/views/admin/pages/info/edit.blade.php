@php
    use App\Http\Helper\Common;
    $table = 'info';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'update') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Save
                    </a>
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chính</a></li>
                            <li class=""><a href="#meta_tab" data-toggle="tab">Thông tin phụ</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-content">
                                <div class="tab-pane active" id="general_tab">
                                    <div class="form-group">
                                        <label for="">Tên công ty<span class="text-danger">*</span></label>
                                        <input id="name" name="name" type="text" class="form-control" value="{{ ($items->name)??old('name') }}">
                                        @if($errors->first('name'))
                                            <p class="text-danger">{{ $errors->first('name') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email chính <span class="text-danger">*</span></label>
                                        <input  name="email" type="text" class="form-control" value="{{ ($items->email)??old('email') }}">
                                        @if($errors->first('email'))
                                            <p class="text-danger">{{ $errors->first('email') }}</p>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="">Số ĐT chính </label>
                                        <input  name="phone" type="tel" class="form-control" value="{{ ($items->phone)??old('phone') }}">
                                        @if($errors->first('phone'))
                                            <p class="text-danger">{{ $errors->first('phone') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Địa chỉ<span class="text-danger">*</span></label>
                                        <input id="name" name="address" type="text" class="form-control" value="{{ ($items->address)??old('address') }}">
                                        @if($errors->first('address'))
                                            <p class="text-danger">{{ $errors->first('address') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Website<span class="text-danger">*</span></label>
                                        <input id="name" name="web" type="text" class="form-control" value="{{ ($items->web)??old('web') }}">
                                        @if($errors->first('web'))
                                            <p class="text-danger">{{ $errors->first('web') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Thời gian làm việc<span class="text-danger">*</span></label>
                                        <input id="name" name="time" type="text" class="form-control" value="{{ ($items->time)??old('time') }}">
                                        @if($errors->first('time'))
                                            <p class="text-danger">{{ $errors->first('time') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nội dung chính<span class="text-danger">*</span></label>
                                        <input id="name" name="contentCty" type="text" class="form-control" value="{{ ($items->content)??old('contentCty') }}">
                                        @if($errors->first('contentCty'))
                                            <p class="text-danger">{{ $errors->first('contentCty') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="">Upload Logo</label>
                                        <div class="action_image">
                                            <button type="button" class="btn btn-info btn_upload">Upload</button>
                                            <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                        </div>
                                        <div class="preview_image">
                                            <img src="{{(old('picture_url')==null)?$items->picture:(old('picture_url'))}}" style="max-width: 200px">
                                            <input type="hidden" name="picture_url" value="{{(old('picture_url')==null)?$items->picture:(old('picture_url'))}}">
                                        </div>
                                        @if($errors->first('picture_url'))
                                            <p class="text-danger">{{ $errors->first('picture_url') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="meta_tab">
                                    <div class="form-group">
                                        <label for="">Email 2 <span class="text-danger">*</span></label>
                                        <input  name="email2" type="text" class="form-control" value="{{ ($items->email2)??old('email2') }}">
                                        @if($errors->first('email2'))
                                            <p class="text-danger">{{ $errors->first('email2') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="">Số ĐT2 </label>
                                        <input  name="phone2" type="tel" class="form-control" value="{{ ($items->phone2)??old('phone2') }}">
                                        @if($errors->first('phone2'))
                                            <p class="text-danger">{{ $errors->first('phone2') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                            <label class="control-label" for="">Upload View-About</label>
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>
                                            <div class="preview_image">
                                                <img src="{{ ($items->pictureView)??old('picture_url2') }}" style="max-width: 200px">
                                                <input type="hidden" name="picture_url2" value="{{(old('picture_url2')==null)?$items->pictureView:(old('picture_url2'))}}">
                                            </div>
                                            @if($errors->first('picture_url2'))
                                                <p class="text-danger">{{ $errors->first('picture_url2') }}</p>
                                            @endif
                                        </div>
                                    <div class="form-group">
                                        <label class="control-label" for="">Upload About</label>
                                        <div class="action_image">
                                            <button type="button" class="btn btn-info btn_upload">Upload</button>
                                            <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                        </div>
                                        <div class="preview_image">
                                            <img src="{{ ($items->pictureAbout)??old('picture_url3') }}" style="max-width: 200px">
                                            <input type="hidden" name="picture_url3" value="{{(old('picture_url3')==null)?$items->pictureAbout:(old('picture_url3'))}}">
                                        </div>
                                        @if($errors->first('picture_url3'))
                                            <p class="text-danger">{{ $errors->first('picture_url3') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="">Upload Contact</label>
                                        <div class="action_image">
                                            <button type="button" class="btn btn-info btn_upload">Upload</button>
                                            <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                        </div>
                                        <div class="preview_image">
                                            <img src="{{ ($items->pictureContact)??old('picture_url4') }}" style="max-width: 200px">
                                            <input type="hidden" name="picture_url4" value="{{(old('picture_url4')==null)?$items->pictureContact:(old('picture_url4'))}}">
                                        </div>
                                        @if($errors->first('picture_url4'))
                                            <p class="text-danger">{{ $errors->first('picture_url4') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="">Upload News</label>
                                        <div class="action_image">
                                            <button type="button" class="btn btn-info btn_upload">Upload</button>
                                            <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                        </div>
                                        <div class="preview_image">
                                            <img src="{{ ($items->pictureNews)??old('picture_url5') }}" style="max-width: 200px">
                                            <input type="hidden" name="picture_url5" value="{{(old('picture_url5')==null)?$items->pictureNews:(old('picture_url5'))}}">
                                        </div>
                                        @if($errors->first('picture_url5'))
                                            <p class="text-danger">{{ $errors->first('picture_url5') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    @include('ckfinder::setup')
@stop

@section('scripts')
    <script>
        var btnUpload = $(".btn_upload");
        var btnRemove= $(".btn_remove");
        btnUpload.on('click', function(){
            selectFileWithCKFinder(this);
        })
        btnRemove.on('click', function(){
            remmoveIMG(this);
        })
        function remmoveIMG(t) {
            $(t).parents(".form-group").children(".preview_image").children("img").attr("src",'/no_image.jpg');
            $(t).parents(".form-group").children(".preview_image").children("input").val('');

        }
        function selectFileWithCKFinder( t) {
            CKFinder.popup( {
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var urlFile = file.getUrl();

                        $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                        $(t).parents(".form-group").children(".preview_image").children("input").val(urlFile);
                    } );

                    finder.on( 'file:choose:resizedImage', function( evt ) {

                    } );
                }
            } );
        }
    </script>
@stop
