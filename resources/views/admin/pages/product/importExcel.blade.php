@php
    use App\Http\Helper\Common;
    $table = 'product';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'storefromExcel') }}" href="javascript:void()"
                       onclick="submitForm(this)"
                       class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Save
                    </a>
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="{{route($href.'uploadfileExcel')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-content">
                                <div class="tab-pane active" id="general_tab">
                                    <div class="form-group">
                                        <label for="">Tên file<span class="text-danger">*</span></label>
                                        <input style="width: 400px;display: inline-block" id="name" name="name" type="file" class="form-control"
                                               value="{{ old('name') }}" accept=".xls,.xlsx,.xlsm,.xltx">
                                        <button type="submit">Upload File</button>
                                    </div>
                                </div>
                            </div>
                </form>
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-pane active" id="general_tab">
                        <div class="form-group">
                            <label for="">Tất cả file<span class="text-danger">*</span></label>
                            <select name="file_name" id="">
                                @foreach($files as $key => $value)
                                    @php
                                        $name = substr($value,7);
                                    @endphp
                                    <option value="{{$value}}">{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    </div>
    </div>
    </div>
@stop

