@php
    use App\Http\Helper\Common;
    $table = 'product';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'update') }}" href="javascript:void()" onclick="submitForm(this)"
                       class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>

                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                            <li class=""><a href="#meta_tab" data-toggle="tab">Meta</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <input type="hidden" name="id" type="text" class="form-control"
                                           value="{{$items->id}}">
                                    <label for="">Tên sản phẩm <span class="text-danger">*</span></label>
                                    <input id="name" name="name" type="text" class="form-control"
                                           value="{{ ($items->name)??old('name') }}">
                                    @if($errors->first('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Giá<span class="text-danger">*</span></label>
                                    <input id="name" name="price" type="number" class="form-control"
                                           value="{{ ($items->price)??old('price') }}">
                                    @if($errors->first('price'))
                                        <p class="text-danger">{{ $errors->first('price') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Discount(%)<span class="text-danger">*</span></label>
                                    <input id="name" name="discount" type="number" class="form-control"
                                           value="{{ ($items->discount)??old('discount') }}">
                                    @if($errors->first('discount'))
                                        <p class="text-danger">{{ $errors->first('discount') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Số lượng<span class="text-danger">*</span></label>
                                    <input id="name" name="quantily" type="number" class="form-control"
                                           value="{{ ($items->quantily)??old('quantily') }}">
                                    @if($errors->first('quantily'))
                                        <p class="text-danger">{{ $errors->first('quantily') }}</p>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" id="status" class="form-control">
                                        @php
                                            $fillter_status = [
                                                    '-1' => '-- Chọn trạng thái --',
                                                    '1' => 'Kích hoạt',
                                                    '0' => 'Không kích hoạt',
                                            ];
                                        $value = (old('status')==null)?$items->status:(old('status'));
                                        @endphp
                                        @foreach($fillter_status as $k => $v)
                                            <option @if($k == $value) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('status'))
                                        <p class="text-danger">{{ $errors->first('status') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Phân loại</label>
                                    <select name="classify" id="status" class="form-control">
                                        @php
                                            $fillter_classify = [
                                                    '-1' => '-- Chọn phân loại --',
                                                    '0' => 'Bình thường',
                                                    '1' => 'Giảm giá',
                                                    '2' => 'Nổi bật',
                                            ];
                                        $value = (old('classify')==null)?$items->classify:(old('classify'));
                                        @endphp
                                        @foreach($fillter_classify as $k => $v)
                                            <option @if($k == $value) selected @endif value="{{ $k }}">{{ $v }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('classify'))
                                        <p class="text-danger">{{ $errors->first('classify') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Danh mục</label>
                                    <select name="category_id" id="" class="form-control">
                                        <option value="-1">-- Chọn trạng thái --</option>
                                        @php
                                            $value  = (old('category_id')==null)?$items->category_id:(old('category_id'));
                                        @endphp
                                        @foreach($subModel as $item)
                                            <option @if($item->id == $value) selected
                                                    @endif value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->first('category_id'))
                                        <p class="text-danger">{{ $errors->first('category_id') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="">Upload image</label>
                                    <div class="action_image">
                                        <button type="button" class="btn btn-info btn_upload">Upload</button>
                                        <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                    </div>
                                    <div class="preview_image" style="margin-top: 10px">
                                        <img src="{{(old('picture_url')==null)?$items->picture:(old('picture_url'))}}"
                                             style="width: 150px;height: 150px">
                                        <input type="hidden" name="picture_url" value="{{$items->picture}}">
                                    </div>
                                    @if($errors->first('picture_url'))
                                        <p class="text-danger">{{ $errors->first('picture_url') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label" for="">Upload Library</label>
                                    </div>
                                    @if($picture_galler!=null)
                                        @foreach($picture_galler as $key => $item)
                                        <div class="form-group" style="display: inline-block">
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>

                                            <div class="preview_image" style="margin-top: 10px">
                                                    <img
                                                            src="{{ ( old('picture_galler[]') == null )? $item : old('picture_galler[]')}}"
                                                        style="width: 150px;height: 150px">
                                                        <input type="hidden" name="picture_galler[]"
                                                           value="{{$item}}">
                                            </div>
                                        </div>
                                        @endforeach
                                    @else
                                        <div class="form-group" style="display: inline-block">
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>
                                            <div class="preview_image" style="margin-top: 10px">
                                                <img
                                                    src="{{(old('picture_galler')==null)?'/no_image.jpg':(old('picture_galler'))}}"
                                                    style="width: 150px;height: 150px">
                                                <input type="hidden" name="picture_galler[]" value="">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: inline-block">
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>
                                            <div class="preview_image" style="margin-top: 10px">
                                                <img
                                                    src="{{(old('picture_galler')==null)?'/no_image.jpg':(old('picture_galler'))}}"
                                                    style="width: 150px;height: 150px">
                                                <input type="hidden" name="picture_galler[]" value="">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: inline-block">
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>
                                            <div class="preview_image" style="margin-top: 10px">
                                                <img
                                                    src="{{(old('picture_galler')==null)?'/no_image.jpg':(old('picture_galler'))}}"
                                                    style="width: 150px;height: 150px">
                                                <input type="hidden" name="picture_galler[]" value="">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: inline-block">
                                            <div class="action_image">
                                                <button type="button" class="btn btn-info btn_upload">Upload</button>
                                                <button type="button" class="btn btn-danger btn_remove">Remove</button>
                                            </div>
                                            <div class="preview_image" style="margin-top: 10px">
                                                <img
                                                    src="{{(old('picture_galler')==null)?'/no_image.jpg':(old('picture_galler'))}}"
                                                    style="width: 150px;height: 150px">
                                                <input type="hidden" name="picture_galler[]" value="">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div><!-- /.tab-pane -->
                            <div class="tab-pane" id="meta_tab">
                                <div class="form-group">
                                    <label for="">Mô tả</label>
                                    <textarea value="" class="form-control" name="contentProduct"
                                              id="ckeditor1">{{ ($items->content)??old('contentProduct') }}</textarea>
                                    @if($errors->first('contentProduct'))
                                        <p class="text-danger">{{ $errors->first('contentProduct') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Slug</label>
                                    <input id="slug" name="slug" type="text" class="form-control"
                                           value="{{(old('slug')==null)?$items->slug:(old('slug'))}}">
                                    @if($errors->first('slug'))
                                        <p class="text-danger">{{ $errors->first('slug') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Meta title</label>
                                    <input name="meta_title" type="text" class="form-control"
                                           value="{{(old('meta_title')==null)?$items->meta_title:(old('meta_title'))}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Meta description</label>
                                    <textarea value="" class="form-control" name="meta_description" cols="30"
                                              rows="5">{{(old('meta_description')==null)?$items->meta_description:(old('meta_description'))}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Meta keywords</label>
                                    <input
                                        value="{{(old('meta_keywords')==null)?$items->meta_keywords:(old('meta_keywords'))}}"
                                        name="meta_keywords" type="text" class="form-control">
                                </div>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div>
                </form>
            </div>

        </div>
    </div>
    @include('ckfinder::setup')

@stop
@section('scripts')
    <script>
        var btnUpload = $(".btn_upload");
        var btnRemove = $(".btn_remove");
        btnUpload.on('click', function () {
            selectFileWithCKFinder(this);
        })
        btnRemove.on('click', function () {
            remmoveIMG(this);
        })

        function remmoveIMG(t) {
            $(t).parents(".form-group").children(".preview_image").children("img").attr("src", '/no_image.jpg');
            $(t).parents(".form-group").children(".preview_image").children("input").val('');

        }

        function selectFileWithCKFinder(t) {
            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        var urlFile = file.getUrl();

                        $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                        $(t).parents(".form-group").children(".preview_image").children("input").val(urlFile);
                    });

                    finder.on('file:choose:resizedImage', function (evt) {

                    });
                }
            });
        }
    </script>
@stop
