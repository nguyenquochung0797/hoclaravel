@php
    use App\Http\Helper\Common;
    $table = 'user';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'store') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Save
                    </a>
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general_tab">
                            <div class="form-group">
                                <label for="">Họ và tên<span class="text-danger">*</span></label>
                                <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
                                @if($errors->first('name'))
                                <p class="text-danger">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Email <span class="text-danger">*</span></label>
                                <input  name="email" type="text" class="form-control" value="{{ old('email') }}">
                                @if($errors->first('email'))
                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Số ĐT</label>
                                <input  name="phone" type="tel" class="form-control" value="{{ old('phone') }}">
                                @if($errors->first('phone'))
                                    <p class="text-danger">{{ $errors->first('phone') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Mật khẩu<span class="text-danger">*</span></label>
                                <input  name="password" type="password" class="form-control" value="">
                                @if($errors->first('password'))
                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Nhập lại mật khẩu<span class="text-danger">*</span></label>
                                <input  name="password_confirm" type="password" class="form-control" value="">
                                @if($errors->first('password_confirm'))
                                    <p class="text-danger">{{ $errors->first('password_confirm') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Quyền</label>
                                <select name="role" id="" class="form-control">
                                    <option value="-1">-- Chọn quyền --</option>
                                    @php
                                        $filter_role = (old('role')==null)?-1:(old('role'));
                                    @endphp
                                    @foreach($subModel as $item)
                                        <option @if($item->id == $filter_role) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->first('role'))
                                    <p class="text-danger">{{ $errors->first('role') }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Trạng thái</label>
                                <select name="status" id="" class="form-control">
                                    @php
                                        $status = [
                                            '-1' => '-- Chọn trạng thái --',
                                            '1' => 'Kích hoạt',
                                            '0' => 'Không kích hoạt',
                                        ];
                                        $stt = (old('status')==null)?-1:(old('status'));
                                    @endphp
                                    @foreach($status as $key => $value)
                                        <option @if($key == $stt) selected @endif value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                @if($errors->first('status'))
                                    <p class="text-danger">{{ $errors->first('status') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </div>


@stop
