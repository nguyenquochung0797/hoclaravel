@php
    use App\Http\Helper\Common;
    $table = 'user';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'changePass') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-plus"></i> Save
                    </a>

                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <input type="hidden" name="id" type="text" class="form-control" value="{{$items->id}}">
                                    <label for="">Mật khẩu<span class="text-danger">*</span></label>
                                    <input  name="password" type="password" class="form-control" value="">
                                    @if($errors->first('password'))
                                        <p class="text-danger">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Nhập lại mật khẩu<span class="text-danger">*</span></label>
                                    <input  name="password_confirm" type="password" class="form-control" value="">
                                    @if($errors->first('password_confirm'))
                                        <p class="text-danger">{{ $errors->first('password_confirm') }}</p>
                                    @endif
                                </div>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div>
                </form>
            </div>

        </div>
    </div>


@stop
