@php
    use App\Http\Helper\Common;
    $table = 'contact';
    $href = 'admin.'.$table.'.';
@endphp
@extends("admin.layout")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body text-center">
                    <a data-link="{{ route($href.'store') }}" href="javascript:void()" onclick="submitForm(this)" class="btn btn-app">
                        <i class="fa fa-trash-o"></i> Save
                    </a>
                    <a class="btn btn-app" href="{{ route($href.'index') }}">
                        <i class="fa fa-trash-o"></i> Back
                    </a>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="box">
                <form action="#" method="POST" id="form_data" enctype="multipart/form-data">
                    @csrf
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_tab" data-toggle="tab">Thông tin chung</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab">
                                <div class="form-group">
                                    <label for="">Họ và tên<span class="text-danger">*</span></label>
                                    <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}">
                                    @if($errors->first('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Email <span class="text-danger">*</span></label>
                                    <input  name="email" type="text" class="form-control" value="{{ old('email') }}">
                                    @if($errors->first('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Số ĐT</label>
                                    <input  name="phone" type="tel" class="form-control" value="{{ old('phone') }}">
                                    @if($errors->first('phone'))
                                        <p class="text-danger">{{ $errors->first('phone') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Content</label>
                                    <textarea value="" class="form-control"  name="contentContact" cols="30" rows="5">{{ old('contentContact') }}</textarea>
                                </div>
                                @if($errors->first('contentContact'))
                                    <p class="text-danger">{{ $errors->first('contentContact') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    @include('ckfinder::setup')
@stop

@section('scripts')
    <script>
        var btnUpload = $(".btn_upload");
        var btnRemove= $(".btn_remove");
        btnUpload.on('click', function(){
            selectFileWithCKFinder(this);
        })
        btnRemove.on('click', function(){
            remmoveIMG(this);
        })
        function remmoveIMG(t) {
            $(t).parents(".form-group").children(".preview_image").children("img").attr("src",'/no_image.jpg');
            $(t).parents(".form-group").children(".preview_image").children("input").val('');

        }
        function selectFileWithCKFinder( t) {
            CKFinder.popup( {
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var urlFile = file.getUrl();

                        $(t).parents(".form-group").children(".preview_image").children("img").attr("src", urlFile);
                        $(t).parents(".form-group").children(".preview_image").children("input").val(urlFile);
                    } );

                    finder.on( 'file:choose:resizedImage', function( evt ) {

                    } );
                }
            } );
        }
    </script>
@stop
